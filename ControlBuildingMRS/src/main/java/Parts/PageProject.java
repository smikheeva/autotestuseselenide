package Parts;

import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class PageProject extends PageObject {
    public PageProject(WebDriver driver) { super (driver); }
    private ArrayList<String> nameEmployee= new ArrayList<>();
    private  String nameOrganization;
    private  String typeOrganization;
    int countProject;
    int numberOrganization;

    public void enterInSystem(String testUserEmail, String testUserPassword) {
        getWhenVisible(By.xpath("//input[@id='Email']"));
        builder.click(driver.findElement(By.xpath("//input[@id='Email']"))).sendKeys(testUserEmail).build().perform();
        builder.click(driver.findElement(By.xpath("//input[@id='Password']"))).sendKeys(testUserPassword).build().perform();
        driver.findElement(By.xpath("//div[3]/button")).click();
    }

    @DisplayName("Открыть страницу проектов")
    public void openProjectPage() throws Exception {
        builder.moveToElement(sidebar).click(projectSidebar).build().perform();
        getWhenVisible(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div"));
        countProject = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div/button")).size();
    }

    @DisplayName("Выбрать проект")
    public void chooseProject(String nameProject) throws Exception {
        Thread.sleep(4000);
        driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div/div/div[1]/p[text()='"+nameProject+"']")).click();
        Thread.sleep(4000);
        if (driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div[1]/div/div[2]/div/p[text()='У вас нет смет']")).size()==1 | driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div[1]/div/div[2]/div/div[2]/div[1]/p[1]")).size()>0){
            System.out.println("Вы выбрали проект '"+nameProject+"'");
        }
        else System.out.println("ERROR!!!");
    }


    @DisplayName("Нажать 'добавить проект' на моей странице")
    public void createProject() throws Exception {
        waitForElementPresence(createProjectButton);
        Thread.sleep(4000);
        createProjectButton.click();
    }

    @DisplayName("Заполнить форму создания нового проекта")
    public void fillingFormNewProject(String ProjectName, String AddressBuilding) throws Exception {
        builder.click(nameProject).sendKeys(ProjectName).build().perform();
        builder.click(addressProject).sendKeys(AddressBuilding).build().perform();
    }

    @DisplayName("Отменить создание нового проекта")
    public void cancelCreateProject() throws Exception {
        cancelCreateProjectButton.click();
    }

    @DisplayName("Подтвердить создание нового проекта")
    public void confirmCreateProject(String ProjectName) throws Exception {
        confirmCreateProjectButton.click();
        checkProject(ProjectName);
        countProject++;

    }

    @DisplayName("Вызвать меню действий над проектом")
    public void actionProject(String ProjectName) throws Exception {
        driver.findElement(By.xpath("//p[text()='" + ProjectName + "']/../../../button")).click();
    }

    @DisplayName("Редактировать проект и отменить")
    public void cancelEditProjectAction(String ProjectName, String EditProjectName) throws Exception {
        driver.findElement(By.xpath("//p[text()='" + ProjectName + "']/../../../button")).click();
        editProjectButton.click();
        nameProject.click();
        nameProject.clear();
        nameProject.sendKeys(EditProjectName);
        cancelEditProjectButton.click();
        checkProject(ProjectName);
        checkAbsenceProject(EditProjectName);
    }

    @DisplayName("Редактировать проект и сохранить изменения")
    public void saveEditProjectAction(String ProjectName, String EditProjectName) throws Exception {
        driver.findElement(By.xpath("//p[text()='" + ProjectName + "']/../../../button")).click();
        editProjectButton.click();
        nameProject.click();
        nameProject.clear();
        nameProject.sendKeys(EditProjectName);
        saveEditProjectButton.click();
        checkProject(EditProjectName);
        checkAbsenceProject(ProjectName);
    }

    @DisplayName("Удалить проект")
    public void deleteProjectAction(String ProjectName) throws Exception {
        driver.findElement(By.xpath("//p[text()='" + ProjectName + "']/../../../button")).click();
        deleteProjectButton.click();
        checkAbsenceProject(ProjectName);
    }

    @DisplayName("Добавить участников к проекту")
    public void addParticipantsAction(String ProjectName) throws Exception {
        driver.findElement(By.xpath("//p[text()='" + ProjectName + "']/../../../button")).click();
        addParticipantsToProjectButton.click();
    }

    @DisplayName("Заполнить форму добавления участников к проекту")
    public void fillingAddParticipantToProject() throws Exception {
        waitForElementPresence(addParticipantToProjectButtonInParticipants);
        Thread.sleep(2000);
        addParticipantToProjectButtonInParticipants.click();
        waitForElementPresence(chooseOrganization);
        chooseOrganization.click();
        int numberOfOrganization = (int) (Math.random()*3)+1;
        Thread.sleep(4000);
        driver.findElement(By.xpath("//div[@id='react-select-2-option-"+numberOfOrganization+"']/div/div")).click();
        waitForElementPresence(driver.findElement(By.xpath("//div[@id='react-select-2-option-"+numberOfOrganization+"']/div/div")));
        nameOrganization=driver.findElement(By.xpath("//div[@id='react-select-2-option-"+numberOfOrganization+"']/div/div")).getText();
        chooseTypeOrganization.click();
        int numberOfTypeOrganization = (int) (Math.random()*7)+1;
        Thread.sleep(4000);
        waitForElementPresence(driver.findElement(By.xpath("//div[@id='react-select-3-option-"+numberOfTypeOrganization+"']/div/div/div/p")));
        driver.findElement(By.xpath("//div[@id='react-select-3-option-"+numberOfTypeOrganization+"']/div/div/div/p")).click();
        typeOrganization=driver.findElement(By.xpath("//div[@id='react-select-3-option-"+numberOfTypeOrganization+"']/div/div/div/p")).getText();
    }

    @DisplayName("Подтвердить добавление участников к проекту")
    public void confirmAddParticipantToProject() throws Exception {
        confirmAddOrganization.click();
        int count = 0;
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        for (int i=0;i<listOrganization.size();i++) {
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[" + (i + 1) + "]/div[1]/div/p[1]")).getText().equals(nameOrganization)
                    && driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[" + (i + 1) + "]/div[1]/div/p[2]")).getText().equals(typeOrganization)) {
                count++;
            }
        }
        if (count>0) System.out.println("Добавлена организация '" + nameOrganization + "' в качестве " + typeOrganization);
        else System.out.println("Организация не добавлена");
    }

    @DisplayName("Отменить добавление участников к проекту")
    public void cancelAddParticipantToProject() throws Exception {
        cancelAddOrganization.click();
        int count = 0;
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        for (int i=0;i<listOrganization.size();i++) {
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[" + (i + 1) + "]/div[1]/div/p[1]")).getText().equals(nameOrganization)
                    && driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[" + (i + 1) + "]/div[1]/div/p[2]")).getText().equals(typeOrganization)) {
                count++;
            }
        }
        if (count==0) System.out.println("Отмена добавление участников к проекту прошла успешно");
        else System.out.println("Отмена добавление участников к проекту не удалась");
    }

    @DisplayName("Выбрать некоторых сотрудиков для добавления")
    public void chooseEmployeeForAdding() throws Exception {
        Thread.sleep(4000);
        int count=0;
        ArrayList<Integer> freeNumber= new ArrayList<>();
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        System.out.println("Всего организаций - " + listOrganization.size());
        numberOrganization = (int) (Math.random() * (listOrganization.size()) + 1);
        System.out.println("numberOrganization - " + numberOrganization);
        driver.findElement(By.xpath("//div/div/div[2]/div["+numberOrganization+"]/div[2]/div[1]/button")).click();
        List<WebElement> listEmployee = driver.findElements(By.xpath("//div[2]/div[3]/div/div[2]/div/div/div[2]/div/span"));
        for (int i=0;i<listEmployee.size();i++) {
            freeNumber.add(i+1);
        }
        int amountEmployee = (int) (Math.random()*(listEmployee.size())+1); //сколько сотрудников добавить
        for (int i=0;i<amountEmployee;i++) {
            int numberEmployee = (int) (Math.random() * freeNumber.size() + 1);
            nameEmployee.add(driver.findElement(By.xpath("//div[2]/div[3]/div/div[2]/div/div/div[2]/div[" + freeNumber.get(numberEmployee-1) + "]/div/div[2]/p[1]")).getText());
            for (int j=0;j<freeNumber.size();j++)
                if (freeNumber.get(j)== numberEmployee-1) freeNumber.remove(i);
            driver.findElement(By.xpath("//div[2]/div[3]/div/div[2]/div/div/div[2]/div[" + numberEmployee + "]/span/span[1]/input")).click();
        }
    }

    @DisplayName("Выбрать всех сотрудиков для добавления")
    public void chooseAllEmployeeForAdding() throws Exception {
        Thread.sleep(4000);
        int count=0;
        ArrayList<Integer> freeNumber= new ArrayList<>();
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        numberOrganization = (int) (Math.random()*listOrganization.size()+1);
        System.out.println("numberOrganization - " + numberOrganization);
        driver.findElement(By.xpath("//div/div/div[2]/div["+numberOrganization+"]/div[2]/div[1]/button")).click();
        List<WebElement> listEmployee = driver.findElements(By.xpath("//div[2]/div[3]/div/div[2]/div/div/div[2]/div/span"));
        driver.findElement(By.xpath("///div[2]/div[3]/div/div[2]/div/div/div[1]/span/span[1]/input")).click();
        for (int i=0;i<listEmployee.size();i++) {
            int numberEmployee = (int) (Math.random() * freeNumber.size() + 1);
            nameEmployee.add(driver.findElement(By.xpath("//div[2]/div[3]/div/div[2]/div/div/div[2]/div[" + freeNumber.get(numberEmployee-1) + "]/div/div[2]/p[1]")).getText());
        }
    }

    @DisplayName("Отмена добавления сотрудников")
    public void cancelAddEmployee() throws Exception {
        int count=0;
        cancelAddEmployeeButton.click();
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        for (int i=0;i<listOrganization.size();i++) {
            for (int j=0;j<nameEmployee.size();j++){
                if (nameEmployee.get(i).equals(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+(i+1)+"]/div[3]/div/div/div/div/div[2]/p[1]")).getText())){
                    count++;
                }
            }
        }
        if (count>0) System.out.println("Отмена добавления сотрудников прошла неудачно");
    }

    @DisplayName("Подтвердить добавления сотрудников")
    public void confirmAddEmployee() throws Exception {
        int count=0;
        confirmAddEmployeeButton.click();
        for (int i=0;i<nameEmployee.size();i++){
            if (nameEmployee.get(i).equals(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+numberOrganization+"]/div[3]/div/div/div/div["+(i+1)+"]/div[2]/p[1]")).getText())){
                count++;
            }
        }
        if (count==nameEmployee.size()) System.out.println("Добавление сотрудников прошло успешно");
    }

    @DisplayName("Редактировать организацию-участника")
    public void editParticipant() throws Exception {

    }

    @DisplayName("Отменить удаление организации-участника")
    public void cancelDeleteParticipant() throws Exception {
        int count = 0;
        String deleteOrganization;
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        numberOrganization = (int) (Math.random() * (listOrganization.size()) + 1);
        deleteOrganization = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+numberOrganization+"]/div[1]/div/p[1]")).getText();
        driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+numberOrganization+"]/div[1]/button")).click();
        driver.findElement(By.xpath("//*[@id='fade-menu']/div[3]/div/div/div[2]")).click();
        driver.findElement(By.xpath("//div[2]/div[3]/div/div[3]/div/button[2]")).click();
        for (int i=0;i<listOrganization.size();i++){
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+(i+1)+"]/div[1]/div/p[1]")).getText().equals(deleteOrganization)){
                count++;
            }
        }
        if (count==1) System.out.println("Отмена удаления '" +deleteOrganization+ "' прошла успешно");
        else System.out.println("Отмена удаления потерпела неудачу");
    }

    @DisplayName("Подтвердить удаление организации-участника")
    public void confirmDeleteParticipant() throws Exception {
        int count = 0;
        String deleteOrganization;
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        numberOrganization = (int) (Math.random() * (listOrganization.size()) + 1);
        deleteOrganization = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+numberOrganization+"]/div[1]/div/p[1]")).getText();
        driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+numberOrganization+"]/div[1]/button")).click();
        driver.findElement(By.xpath("//*[@id='fade-menu']/div[3]/div/div/div[2]")).click();
        driver.findElement(By.xpath("//div[2]/div[3]/div/div[3]/div/button[1]")).click();
        Thread.sleep(4000);
        for (int i=0;i<listOrganization.size();i++){
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+(i+1)+"]/div[1]/div/p[1]")).getText().equals(deleteOrganization)){
                count++;
            }
        }
        System.out.println(count);
        if (count==0) System.out.println("Удаление '" +deleteOrganization+ "' прошло успешно");
        else System.out.println("Удаление потерпело неудачу");
    }


    @DisplayName("Проверить отсутствие записи проекта")
    public void checkAbsenceProject (String ProjectName){
        int countProjectName=0;
        for (int i=0;i<countProject;i++) {
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div["+(i+1)+"]/div/div/div[1]/p")).getText().equals(ProjectName)){
                countProjectName=1;
                i=driver.findElements(By.xpath("//div/button/span")).size();
            }
        }
        if (countProjectName==0) System.out.println("Проект '" + ProjectName + "' не существует!");
        else System.out.println("Проверка отсутствие записи выявила ошибку!");
    }

    @DisplayName("Проверить наличие записи проекта")
    public void checkProject (String ProjectName){
        int countProjectName=0;
        for (int i=0;i<countProject;i++) {
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div["+(i+1)+"]/div/div/div[1]/p")).getText().equals(ProjectName)){
                countProjectName++;
                i=driver.findElements(By.xpath("//div/button/span")).size();
            }
        }
        if (countProjectName>0) System.out.println("Проект '" + ProjectName + "' существует!");
        else System.out.println("Проверка наличия записи выявила ошибку!");
    }
}
