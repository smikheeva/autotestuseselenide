package Parts;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PagePsd extends PageObject {
    public PagePsd (WebDriver driver) { super (driver); }
    private String nameFolder;
    int countFolder;
    int countEstimatesInFolder;
    ArrayList<String> listEstimateInProject = new ArrayList<>();
    ArrayList<String> validForImportEstimateInFolder = new ArrayList<>();
    ArrayList<String> selectedForImportEstimateInFolder = new ArrayList<>();
    ArrayList<String> importedEstimates = new ArrayList<>();
    String randomEstimateInFolder;

    @DisplayName("Открыть страницу проектно-сметной документации")
    public void openPSD() throws Exception {
        builder.moveToElement(sidebar).click(PSDsidebar).build().perform();
        waitForElementPresence(createFolderInPSD);
        if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[1]/div[1]/div/p")).getText().equals("Проектно-сметная документация")) {
            System.out.println("Вы перешли на страницу проектно-сметной документации");
        }
        else System.out.println("ОШИБКА!!! Переход на страницу в проектно-сметной документации не осуществлён!!!");
        builder.moveByOffset(1000, 0).perform();
    }

    @DisplayName("Получаю список смет в прогрессе работ")
    public void getEstimates() throws Exception {
        var work = new PageWorkProgress(driver);
        work.openProgressWork();
        Thread.sleep(4000);
        List<WebElement> listEstimates = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div[1]/div/div[2]/div/div[2]/div[1]/p[1]"));
        System.out.println("Количество смет в проекте: " + listEstimates.size());
        if (driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div[1]/div[2]/div[1]/p[1]")).size()>0) {
            for (int i = 0; i < listEstimates.size(); i++) {
                listEstimateInProject.add(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div[1]/div/div[2]/div[" + (i + 1) + "]/div[2]/div[1]/p[1]")).getText());
            }
        }
        Thread.sleep(4000);
    }

    @DisplayName("Создать новую папку")
    public void createNewFolder(Integer countFolder) throws Exception {
        int count=0;
        for (int i=0;i<countFolder;i++) {
            Thread.sleep(4000);
            waitForElementPresence(createFolderInPSD);
            createFolderInPSD.click();
            builder.sendKeys(""+Math.random() + (i+1)).sendKeys(Keys.ENTER).build().perform();
            builder.sendKeys(Keys.ENTER).build().perform();
            Thread.sleep(4000);
            //waitForElementPresence(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div//p[contains(.,'"+Math.random()+(i+1)+"')]")));
            count++;
        }
        if (count==countFolder) System.out.println("Создание "+countFolder+" папок прошло успешно");
        Assert.assertTrue("ОШИБКА!!! Создание прошло неудачно!", count==countFolder);

    }

    @DisplayName("Выбрать рандомную папку и открыть")
    public void selectRandomFolder() throws Exception {
        Thread.sleep(4000);
        waitForElementPresence(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div/div")));
        List<WebElement> listFolder = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div/div"));
        int randomFolder = (int) (Math.random() * listFolder.size() + 1);
        driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div/div[" + randomFolder + "]/div[1]")).click();
        Thread.sleep(4000);
        waitForElementPresence(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[1]/p[text()='" + driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div/div[" + randomFolder + "]/div[1]/span[2]/p")).getText() + "']")));
    }

    @DisplayName("Редактировать рандомную папку")
    public void editRandomFolder() throws Exception {
        List<WebElement> listFolder = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div/div"));
        int randomFolder = (int) (Math.random()*listFolder.size()+1);
        nameFolder = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div["+randomFolder+"]/div")).getText();
        countFolder = driver.findElements(By.xpath("//p[contains(.,'"+nameFolder+"')]")).size();
        System.out.println("Редактирую папку '" + nameFolder + "'");
        Thread.sleep(4000);
        waitForElementPresence(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div["+randomFolder+"]/div")));
        builder.moveToElement(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div["+randomFolder+"]/div"))).perform();
        builder.click(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div["+randomFolder+"]/div/button"))).perform();
        Thread.sleep(4000);
        waitForElementPresence(editFolderInPSD);
        editFolderInPSD.click();
        //nameFolderField.clear();
        nameFolderField.sendKeys("папка");
        nameFolderField.sendKeys(Keys.ENTER);
        Thread.sleep(4000);
        //waitForElementPresence(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div//p[contains(.,'55 папка')]")));
        //if (countFolder == driver.findElements(By.xpath("//p[contains(.,'"+nameFolder+"')]")).size()+1 && driver.findElements(By.xpath("//p[contains(.,'55 папка')]")).size()==1) System.out.println("Редактирование папки прошло успешно");
        //Assert.assertTrue("ОШИБКА!!! Папка '" + nameFolder + "' не отредактирована!!!", (countFolder == driver.findElements(By.xpath("//p[contains(.,'"+nameFolder+"')]")).size()+1) && driver.findElements(By.xpath("//p[contains(.,'55 папка')]")).size()==1);
    }

    @DisplayName("Удалить рандомную папку")
    public void deleteRandomFolder() throws Exception {
        List<WebElement> listFolder = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div/div"));
        int randomFolder = (int) (Math.random()*listFolder.size()+1);
        nameFolder = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div["+randomFolder+"]/div")).getText();
        countFolder = driver.findElements(By.xpath("//p[contains(.,'"+nameFolder+"')]")).size();
        System.out.println("Удаляю папку '" + nameFolder + "'");
        builder.moveToElement(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div["+randomFolder+"]/div"))).perform();
        Thread.sleep(4000);
        waitForElementPresence(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div["+randomFolder+"]/div[1]/button")));
        builder.click(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div["+randomFolder+"]/div/button"))).perform();
        deleteFolderInPSD.click();
        Thread.sleep(4000);
        waitForElementNotPresented(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div["+randomFolder+"]/div/button"));
        Assert.assertTrue("ОШИБКА!!! Папка '" + nameFolder + "' не удалена!!!", countFolder == driver.findElements(By.xpath("//p[contains(.,'"+nameFolder+"')]")).size()+1);
        if (countFolder == driver.findElements(By.xpath("//p[contains(.,'"+nameFolder+"')]")).size()+1) System.out.println("Удаление папки прошло успешно");
    }

    @DisplayName("Добавить папку в рандомную папку")
    public void addFolderToFolder(String addFolder) throws Exception {
        List<WebElement> listFolder = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div/div"));
        int randomFolder = (int) (Math.random()*listFolder.size()+1);
        nameFolder = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div["+randomFolder+"]/div")).getText();
        countFolder = driver.findElements(By.xpath("//*[.='"+addFolder+"']")).size();
        System.out.println("Добавляю папку в папку '" + nameFolder + "'");
        builder.moveToElement(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div//*[.='"+nameFolder+"']"))).perform();
        waitForElementPresence(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div/div["+randomFolder+"]/div/button")));
        builder.click(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div["+randomFolder+"]/div/button"))).perform();
        addFolderInPSD.click();
        builder.sendKeys(addFolder).sendKeys(Keys.ENTER).build().perform();
        waitForElementPresence(driver.findElement(By.xpath("//*[.='"+addFolder+"']")));
        Assert.assertTrue("ОШИБКА!!! Папка '" + addFolder + "' не добавлена!!!", countFolder+4 == driver.findElements(By.xpath("//*[.='"+addFolder+"']")).size());
        if (countFolder+4 == driver.findElements(By.xpath("//*[.='"+addFolder+"']")).size()) System.out.println("Добавление папки прошло успешно");
    }

    @DisplayName("получаю сегодняшнюю дату")
    public String getToday() throws Exception {
        String todayMonth = "";
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        String todayDay = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
        switch (month) {
            case 0:
                todayMonth = "января";
                break;
            case 1:
                todayMonth = "февраля";
                break;
            case 2:
                todayMonth = "марта";
                break;
            case 3:
                todayMonth = "апреля";
                break;
            case 4:
                todayMonth = "мая";
                break;
            case 5:
                todayMonth = "июня";
                break;
            case 6:
                todayMonth = "июля";
                break;
            case 7:
                todayMonth = "августа";
                break;
            case 8:
                todayMonth = "сентября";
                break;
            case 9:
                todayMonth = "октября";
                break;
            case 10:
                todayMonth = "ноября";
                break;
            case 11:
                todayMonth = "декабря";
                break;
        }
        String today = todayDay.concat(" ").concat(todayMonth);
        return today;
    }

    @DisplayName("Загрузить файл в папку")
    public void loadFile(String estimateLink) throws Exception {
        String[] nameLoadEstimate = estimateLink.split("/");
        int countLoadFiles = 0;
        waitForElementPresence(loadFileButton);
        Thread.sleep(4000);
        loadFileField.sendKeys(estimateLink);
        Thread.sleep(20000);
        waitForElementPresence(driver.findElement(By.xpath("//*[contains(.,'"+nameLoadEstimate[7]+"')]")));
        List<WebElement> listFiles = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div"));
        for (int i=0;i<listFiles.size();i++) {
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[" + (i + 1) + "]/p[1]")).getText().equals(nameLoadEstimate[7]) &&
                    driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[" + (i + 1) + "]/p[3]")).getText().equals(getToday())) {
                countLoadFiles++;
            }
        }
        Assert.assertTrue("Файл не загружен", countLoadFiles == 1);
        if (countLoadFiles == 1) System.out.println("Загрузка файла '"+nameLoadEstimate[7]+"' прошла успешно");
    }

    @DisplayName("Выбрать смету в папке")
    public void selectEstimateInPSD() throws Exception {
        System.out.println("Всего смет в папке: " + countEstimatesInFolder);
        int numberRandomEstimateInFolder = (int) (Math.random()*countEstimatesInFolder+1);
        driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div["+numberRandomEstimateInFolder+"]")).click();
        randomEstimateInFolder = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div["+numberRandomEstimateInFolder+"]/p[1]")).getText();
        System.out.println("Выбранная смета в папке: " + randomEstimateInFolder);
    }

    @DisplayName("Удалить файл из папки")
    public void deleteFile() throws Exception {
        int count=0;
        System.out.println("Всего смет в папке: " + countEstimatesInFolder);
        int numberRandomEstimateInFolder = (int) (Math.random()*countEstimatesInFolder+1);
        driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div["+numberRandomEstimateInFolder+"]")).click();
        randomEstimateInFolder = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div["+numberRandomEstimateInFolder+"]/p[1]")).getText();
        deleteFilesButton.click();
        countEstimatesInFolder = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div")).size();
        Thread.sleep(8000);
        for (int i=0;i<countEstimatesInFolder;i++) {
            if (randomEstimateInFolder.equals(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[" + (i + 1) + "]/p[1]")).getText())){
                count++;
            }
        }
        Assert.assertTrue("ОШИБКА!!! Файл"+randomEstimateInFolder+" не был удалён!!!", count==0);
    }


    @DisplayName("Выбрать и импортировать смету")
    public void importEstimateInPSD() throws Exception {
        int countImportedEstimates = 0;
        validForImportEstimateInFolder.clear();
        getEstimates();
        openPSD();
        selectRandomFolder();
        countEstimatesInFolder = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div")).size();
        for (int i=0;i<countEstimatesInFolder;i++) {
            int count=0;
            for (int j=0;j<listEstimateInProject.size();j++) {
                if ((listEstimateInProject.get(j).concat(".arp")).equals(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[" + (i + 1) + "]/p[1]")).getText())) {
                    count++;
                }
            }
            if (count==0){
                validForImportEstimateInFolder.add(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[" + (i + 1) + "]/p[1]")).getText());
            }
        }
        if (validForImportEstimateInFolder.size()==0) System.out.println("Все сметы из этой папки уже были импортированы!");
        else {
            randomEstimateInFolder = validForImportEstimateInFolder.get((int) (Math.random() * validForImportEstimateInFolder.size()));
            driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div/p[1][contains(.,'" + randomEstimateInFolder + "')]")).click();
            listEstimateInProject.add(randomEstimateInFolder);
            System.out.println("Импортирование сметы '"+randomEstimateInFolder+"'");
            waitForElementPresence(importFilesButton);
            importFilesButton.click();
            waitForElementPresence(goToProgressWorkYes);
            Thread.sleep(20000);
            goToProgressWorkYes.click();
            Thread.sleep(4000);
            waitForElementPresence(additionalWorksTab);
            List<WebElement> listEstimates = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div"));
            System.out.println("Количество смет в проекте: " + listEstimates.size());
            for (int i = 0; i < listEstimates.size(); i++) {
                if ((driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div[" + (i + 1) + "]/div[2]/div[1]/p[1]")).getText().concat(".arp")).equals(randomEstimateInFolder)) {
                    countImportedEstimates++;
                    i = listEstimates.size();
                }
            }
            Assert.assertTrue("ОШИБКА!!! При импортировании смет произошла ошибка!", countImportedEstimates == 1);
            if (countImportedEstimates == 1)
                System.out.println("Импорт выбранных смет прошел успешно");
        }
    }

    @DisplayName("Выбрать и импортировать несколько смет")
    public void importSomeEstimatesInPSD() throws Exception {
        getEstimates();
        openPSD();
        selectRandomFolder();
        int countImportedEstimates = 0;
        int randomCount = 0;
        selectedForImportEstimateInFolder.clear();
        validForImportEstimateInFolder.clear();
        countEstimatesInFolder = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div")).size();
        for (int i=0;i<countEstimatesInFolder;i++) {
            int count=0;
            for (int j=0;j<listEstimateInProject.size();j++) {
                if ((listEstimateInProject.get(j).concat(".arp")).equals(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[" + (i + 1) + "]/p[1]")).getText())) {
                    count++;
                }
            }
            if (count==0){
                validForImportEstimateInFolder.add(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[" + (i + 1) + "]/p[1]")).getText());
            }
        }
        if (validForImportEstimateInFolder.size()==0) System.out.println("Все сметы из этой папки уже были импортированы!");
        else {
            randomCount = (int) (Math.random() * validForImportEstimateInFolder.size())+1;
            System.out.println("Импортирую "+randomCount+" смет");
            for (int j=0;j<randomCount;j++){
                randomEstimateInFolder = validForImportEstimateInFolder.get((int) (Math.random() * validForImportEstimateInFolder.size()));
                validForImportEstimateInFolder.remove(randomEstimateInFolder);
                selectedForImportEstimateInFolder.add(randomEstimateInFolder);
                driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div/p[1][contains(.,'" + randomEstimateInFolder + "')]")).click();
                System.out.println("Импортирование сметы '"+randomEstimateInFolder+"'");
            }
            waitForElementPresence(importFilesButton);
            importFilesButton.click();
            Thread.sleep(20000);
            waitForElementPresence(goToProgressWorkYes);
            goToProgressWorkYes.click();
            Thread.sleep(4000);
            waitForElementPresence(additionalWorksTab);
            List<WebElement> listEstimates = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div"));
            System.out.println("Количество смет в проекте: " + listEstimates.size());
            for (int i = 0; i < selectedForImportEstimateInFolder.size(); i++) {
                for (int j=0;j<listEstimates.size(); j++) {
                    if ((driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div[" + (j+1) + "]/div[2]/div[1]/p[1]")).getText().concat(".arp")).equals(selectedForImportEstimateInFolder.get(i))) {
                        countImportedEstimates++;
                        j = listEstimates.size();
                    }
                }
            }
            Assert.assertTrue("ОШИБКА!!! При импортировании смет произошла ошибка!", countImportedEstimates == selectedForImportEstimateInFolder.size());
            if (countImportedEstimates == selectedForImportEstimateInFolder.size())
                System.out.println("Импорт выбранных смет прошел успешно");
        }
    }


    @DisplayName("Для проверки")
    public void check() throws Exception {
        var work = new PageWorkProgress(driver);
        work.openProgressWork();
        List<WebElement> listEstimates = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div"));
        for (int i=0;i<listEstimates.size();i++) {
            System.out.println(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div["+ (i+1) + "]/div[2]/div[1]/p[1]")).getText());
        }
    }
}
