package Parts;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class PageArea extends PageObject{
    public PageArea (WebDriver driver) { super (driver);}

    @DisplayName("Перейти к Участкам на боковой панели")
    public void openAreaSideBar (){
        builder.moveToElement(sidebar).click(area).build().perform();
    }

    @DisplayName("Открыть объект")
    public void openTheObject(String objectTitle){
        driver.findElement(By.xpath("//*[contains(text(),'" + objectTitle + "')]")).click();
    }

    @DisplayName("Отменить создание нового объекта")
    public void cancelAddingObjectButton (){
        addObjectButton.click();
        cancelButton.click();
    }

    @DisplayName("Добавить объект")
    public void addObjectNewBuilding (String objectTitle, String objectAddress){
        addObjectButton.click();
        builder.click(nameObject).sendKeys(objectTitle).build().perform();
        builder.click(addressObject).sendKeys(objectAddress).build().perform();
        createObjectButton.click();
    }

    @DisplayName("Проверить создание объекта")
    public void checkCreatingObject (String objectTitle){
        List<WebElement> list2 = driver.findElements(By.xpath("//*[contains(text(),'" + objectTitle + "')]"));
        Assert.assertTrue("Object not created!",list2.size() > 0);
    }

    @DisplayName("Проверить отмену создания объекта")
    public void checkCancelingCreateObject (String objectTitle){
        List<WebElement> list2 = driver.findElements(By.xpath("//*[contains(text(),'" + objectTitle + "')]"));
        Assert.assertTrue("Object created!",list2.size() == 0);
    }

    @DisplayName("Заполнить поля создания нового объекта")
    public void fillFormCreateObject (String objectTitle, String objectAddress){
        builder.click(nameObject).sendKeys(objectTitle).build().perform();
        builder.click(addressObject).sendKeys(objectAddress).build().perform();
    }

    @DisplayName("Создать участки")
    public void createAreaIntoArea (int i){
        createAreasButton.click();
        titleArea.sendKeys("" + i + "участок");
    }

    @DisplayName("Создать участки")
    public void createManyAreas (String titleObject, int i) throws Exception{
        driver.findElement(By.xpath("//p[text()='" + titleObject + "']/../..//button")).click();
        createAreasButton.click();
        titleArea.sendKeys("1 участок");
        titleArea.sendKeys(Keys.ENTER);
        for (int j=1;j<i;j++)
        {
            driver.findElement(By.xpath("//p[text()='" + j + " участок']/../..//button")).click();
            createAreasButton.click();
            int a=j+1;
            titleArea.sendKeys("" + a + " участок");
            titleArea.sendKeys(Keys.ENTER);
        }
    }


    @DisplayName("Открыть участок")
    public void openTheArea (String areaTitle){
        driver.findElement(By.xpath("//*[contains(text(),'" + areaTitle + "')]")).click();
    }

    @DisplayName("Проверить разбиение объекта на участки")
    public void checkDiviningIntoAreas(String areaTitle, Integer areaNumber){
        var area = new PageArea(driver);
        area.openTheArea(areaTitle);
        List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + areaTitle + "')]"));
        Assert.assertTrue("Areas not created!",list.size() == areaNumber);
    }
}
