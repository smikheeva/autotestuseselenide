package Parts;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class PageFeed extends PageObject{
    public PageFeed (WebDriver driver) { super (driver);}
    public int waitTime = 1000;

    @DisplayName("Проверка наличия системного сообщения после создания новой дополнительной работы")
    public void viewSystemMessageAboutNewWork (String titleNewWork) throws Exception{
        Thread.sleep(8000);
        driver.findElement(By.xpath("//*[contains(text(),'" + titleNewWork + "')]")).click();
        List<WebElement> list = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[2]/div/div[2]//*[contains(text(),'добавил работу ')]"));
        Assert.assertTrue("Comment create new work not found!", list.size() > 0);
    }

    @DisplayName("Проверка наличия системного сообщения после подтверждения выполнения объёма работы")
    public void viewSystemComplectedVolumeMessage (String Volume) {
        List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'выполнил " + Volume + "')]"));
        Assert.assertTrue("Comment complected volume not found!", list.size() > 0);
    }

    @DisplayName("Проверка отсутствия системного сообщения после подтверждения выполнения объёма работы")
    public void absenceSystemComplectedVolumeMessage (String Volume) {
        List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'выполнил " + Volume + "')]"));
        Assert.assertTrue("Comment complected volume is found!", list.size() == 0);
    }

    public void viewSystemProgressMessage () {}

    public void viewSystemProgressMessageWithPhoto () {}


    @DisplayName("Написать и отправить текстовый комментарий")
    public void sendTextComment (String MyComment) throws Exception{
        builder.click(workCommentField).sendKeys(MyComment).sendKeys(Keys.ENTER).build().perform();
    }

    @DisplayName("Написать и отправить текстовый комментарий прикрепленным файлом")
    public void sendTextCommentWithFile (String MyComment, String FileName) throws Exception{
        builder.click(workCommentField).sendKeys(MyComment).build().perform();
        builder.sendKeys(workAddFileToComment, FileName).build().perform();
        Thread.sleep(waitTime);
        workSendComment.click();
        Thread.sleep(waitTime);
    }

    @DisplayName("Проверить наличие комментария")
    public void checkComment (String MyComment) throws Exception{
        List<WebElement> list = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[2]/div/div[2]//*[contains(text(),'" + MyComment + "')]"));
        Assert.assertTrue("COMMENT NOT FOUND", list.size() > 0);
    }
}

