package Parts;

import org.apache.commons.math3.util.Precision;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class PageWorkProgress extends PageObject {
    public PageWorkProgress (WebDriver driver) { super (driver); }

    public int waitTime = 1000;

    private String warningForUser = "превышение планового объема";
    private String btnUpdate = "Редактировать запись";
    private Double fullVolumeInDetailWork;
    private Double doneVolumeInDetailWork;
    private Double leftVolumeInDetailWork;
    private int randomNumberWork;
    private int randomSection;
    private int count;
    Double volume;
    ArrayList<Integer> countWorkInSection = new ArrayList<>();
    ArrayList<String> selectWork = new ArrayList<>();
    ArrayList<String> selectEstimate = new ArrayList<>();
    private String randomWork;
    String estimateTitle;
    String workTitle;
    private ArrayList<String> selectedWork = new ArrayList<>();

    @DisplayName("Перейти к прогрессу работ")
    public void openProgressWork () throws Exception {
        waitForElementPresence(driver.findElement(By.xpath("//*[@id='app']/div/div/div/div/div/ul/div[2]/div")));
        builder.moveToElement(sidebar).click(workProgressSidebar).build().perform();
        builder.moveByOffset(1000, 0).perform();
    }

    @DisplayName("Заполнить поля добавления записи о выполнении объёма работы")
    public void fillingRecordOnWorkWithoutPhoto () throws Exception {
        addingWorkRecord.click();
        waitForElementPresence(enterVolumeWork);
        leftVolume();
        enterVolumeWork.clear();
        volume = 0.0;
        while (volume<0.000001){
            volume = (Precision.round(Math.random()* leftVolumeInDetailWork, 6));
        }
        enterVolumeWork.sendKeys(Double.toString(volume));
    }

    @DisplayName("Подтвердить введенный объём")
    public void confirmAddingRecordOnWork () throws Exception {
        fillingRecordOnWorkWithoutPhoto();
        waitForElementPresence(sendToNotebook);
        sendToNotebook.click();
        String lastEnteredVolume = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div/div[1]/div[2]/div/p[1]")).getText();
        checkPresenceWorkInNotebook();
        Assert.assertTrue("ОШИБКА!!! Подтверждение прошло неудачно", count==1 && lastEnteredVolume.equals(Double.toString(volume).concat(" ")));
        openProgressWork();
    }

    @DisplayName("Отменить введенный объём")
    public void cancelAddingRecordToWork () throws Exception {
        String lastEnteredVolume = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div/div[1]/div[2]/div/p[1]")).getText();
        fillingRecordOnWorkWithoutPhoto();
        waitForElementPresence(cancelAddingRecord);
        cancelAddingRecord.click();
        Assert.assertTrue("ОШИБКА!!! Отмена прошла неудачно", driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div/div[1]/div[2]/div/p[1]")).getText().equals(lastEnteredVolume));
        checkPresenceWorkInNotebook();
        Assert.assertTrue("ОШИБКА!!! Отмена прошла неудачно", count==0);
        openProgressWork();
    }

    @DisplayName("Проверить наличие работы в журнале")
    public void checkPresenceWorkInNotebook () throws Exception {
        count=0;
        builder.moveToElement(sidebar).click(workJournal).build().perform();
        builder.moveByOffset(1000, 0).perform();
        waitForElementPresence(confirmWork);
        for (int i=0;i<driver.findElements(By.xpath("//div/span")).size();i++){
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div[3]/div["+(i+1)+"]/div/span/../..//div[2]/p")).getText().equals(workTitle) &&
                    driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div[3]/div["+(i+1)+"]/div/span/../..//div[2]/div/p[2]")).getText().equals(Double.toString(volume))) count++;
        }
    }


    @DisplayName("Считать объёмы работы")
    public void leftVolume() throws Exception {
        waitForElementPresence(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/div/div[1]/div")));
        fullVolumeInDetailWork = Double.parseDouble((driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/div/div[1]/div/p[2]")).getText()).split("\\ ", 2)[0]);
        doneVolumeInDetailWork = Double.parseDouble((driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/p[2]")).getText()).split("\\ ", 2)[0]);
        leftVolumeInDetailWork = fullVolumeInDetailWork - doneVolumeInDetailWork;
    }

    @DisplayName("Ввести объём больше планового")
    public void addVolumeMoreThenPlaned () throws Exception {
        waitForElementPresence(addingWorkRecord);
        addingWorkRecord.click();
        leftVolume();
        String WorkValue = Double.toString(leftVolumeInDetailWork+1);
        enterVolumeWork.click();
        enterVolumeWork.sendKeys(WorkValue);
        System.out.println("Вводимый объём: " + WorkValue);

    }

    @DisplayName("Ввести объём меньше 0")
    public void addVolumeSubzero () throws Exception {
        addingWorkRecord.click();
        leftVolume();
        String WorkValue = "-".concat(Double.toString(leftVolumeInDetailWork));
        enterVolumeWork.click();
        enterVolumeWork.sendKeys(WorkValue);
        System.out.println("Вводимый объём: " + WorkValue);
        System.out.println("Вводимый объём должен быть больше 0!!!");
        //assertFalse(sendToNotebook.isEnabled());
    }

    @DisplayName("Заполнить поля создания новой работы")
    public void fillingFieldsInNewWork (String workName, String workVolume) throws Exception{
        additionalWorksTab.click();
        waitForElementPresence(createWorkBtn);
        createWorkBtn.click();
        builder.click(titleNewWork).sendKeys(workName).build().perform();
        builder.click(valueNewWork).sendKeys(workVolume).build().perform();
        unitsNewWork.click();
        int randomUnit = (int) (Math.random() * 6) +1;
        driver.findElement(By.xpath("//*[@id='menu-']/div[3]/ul/li["+ randomUnit + "]")).click();
    }

    @DisplayName("Отменить создание новой работы")
    public void cancelCreatingNewWorkWithoutComment (String titleNewWork) throws Exception {
        fillingFieldsInNewWork(titleNewWork, "100");
        waitForElementPresence(cancelAddNewWork);
        Thread.sleep(4000);
        cancelAddNewWork.click();
        List<WebElement> list = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[2]//*[contains(text(),'"+titleNewWork+"')]"));
        Assert.assertTrue("ОШИБКА!!! Отмена создания работы '" + titleNewWork + "' потерпела неудачу!!!", list.size() == 0);
    }

    @DisplayName("Подтвердить создание новой работы")
    public void confirmCreatingNewWorkWithoutComment (String titleNewWork) throws Exception {
        fillingFieldsInNewWork(titleNewWork, "1000");
        waitForElementPresence(applyAddNewWork);
        Thread.sleep(4000);
        applyAddNewWork.click();
        List<WebElement> list = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[2]//*[contains(text(),'"+titleNewWork+"')]"));
        Assert.assertTrue("ОШИБКА!!! Работа '" + titleNewWork + "' не создана!!!", list.size() > 0);
    }

    @DisplayName("Проверяет цвет элемента")
    public void colorIssueCard(String colorBtn) {
        String btnColor = driver.findElement(
                By.xpath("xxx"))
                .getCssValue("border-left-color");
        assertEquals(btnColor, colorBtn);
    }

    @DisplayName("Выбираю рандомную смету")
    public void selectRandomEstimate () throws Exception {
        waitForElementPresence(additionalWorksTab);
        List<WebElement> listEstimates = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div"));
        System.out.println("Всего смет в списке: " +listEstimates.size());
        int randomEstimate = (int) (Math.random() * listEstimates.size()+1);
        String estimateTitle = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div["+ (randomEstimate) + "]/div[2]/div[1]/p[1]")).getText();
        System.out.println("Выбранная смета: " + estimateTitle);
        driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div["+ randomEstimate + "]")).click();
    }

    @DisplayName("вернуться к списку смет")
    public void returnToListOfEstimates () throws Exception {
        waitForElementPresence(returnToListOfEstimatesButton);
        returnToListOfEstimatesButton.click();
    }

    @DisplayName("Выбираю рандомную смету и рандомную работу")
    public void selectRandomEstimateAndWork () throws Exception {
        waitForElementPresence(driver.findElement(By.xpath("//p[text()='№1']")));
        ArrayList<String> validEstimate = new ArrayList<>();
        ArrayList<String> validWork = new ArrayList<>();
        List<WebElement> listEstimate = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div//*[contains(text(),'%')]"));
        for (int i=0;i<listEstimate.size();i++) {
            validEstimate.add(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[2]/div[" + (i + 1) + "]/div[2]/div[1]/p[1]")).getText());
        }
        for (int i=0;i<validEstimate.size();i++) {
            int randomEstimateIndex = (int) (Math.random() * (validEstimate.size()));
            estimateTitle = validEstimate.get(randomEstimateIndex);
            driver.findElement(By.xpath("//*[contains(text(),'" + estimateTitle + "')]")).click();
            Thread.sleep(4000);
            if ((driver.findElement(By.xpath("//*[contains(text(),'" + estimateTitle + "')]/../../../div[1]")).getText().split("\n", 0)[0]).equals("100")) {
                validEstimate.remove(randomEstimateIndex);
            }
            else {
                int countSection = driver.findElements(By.xpath("//p[text()='№1']")).size();
                if (countSection == 0 || countSection == 1) {
                    int countWork = driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[2]/div//div/div[1]/div/p[contains(text(),'№')]")).size();
                    for (int j=0;j<countWork;j++) {
                        validWork.add(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[2]/div/div["+(j+1)+"]/div/div[1]/p")).getText());
                    }
                }
                else {
                    for (int j = 0; j < countSection; j++) {
                        for (int k=0;k<driver.findElements(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[2]/div[" + (j+1) + "]//*[contains(text(),'№')]")).size();k++){
                            validWork.add(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div[2]/div[" + (j+1) + "]/div[" + (k+1) + "]/div/div[1]/p")).getText());
                        }
                    }
                }
                while (validWork.size()>0){
                    int randomWorkIndex = (int) (Math.random() * (validWork.size()));
                    workTitle = validWork.get(randomWorkIndex);
                    driver.findElement(By.xpath("//*[contains(text(),'" + workTitle + "')]")).click();
                    leftVolume();
                    if (leftVolumeInDetailWork<0.00002) {
                        validWork.remove(randomWorkIndex);
                        if (validWork.size()==1) {
                            validEstimate.remove(randomEstimateIndex);
                            returnToListOfEstimates();
                        }
                    }
                    else {
                        System.out.println("Выбранная смета: " + estimateTitle);
                        System.out.println("Выбранная работа: " + workTitle);
                        i = validEstimate.size();
                        break;
                    }
                }
            }
        }
    }


    @DisplayName("Для проверки")
    public void check () throws Exception {

    }
}

