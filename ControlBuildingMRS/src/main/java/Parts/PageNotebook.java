package Parts;

import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PageNotebook extends PageObject {
    public PageNotebook (WebDriver driver) { super (driver);}
    ArrayList<String> workBeforeConfirm = new ArrayList<>();                  //названия работ до подтверждения в блокноте
    ArrayList<String> workAfterConfirm = new ArrayList<>();                   //названия работ после подтверждения в журнале
    ArrayList<String> uncheckedWork = new ArrayList<>();                      //названия неотмеченных работ в блокноте
    public ArrayList<String> checkedWork = new ArrayList<>();                 //названия отмеченных и подтвержденных работ
    ArrayList<String> unconfirmedVolume = new ArrayList<>();                  //неподтвержденный объём работы
    public ArrayList<String> checkedVolume = new ArrayList<>();               //отмеченный и подтвержденный объём работы за сегодня
    public ArrayList<String> checkedFullVolume = new ArrayList<>();           //отмеченный и подтвержденный объём работы за всё время
    ArrayList<String> volumeInJournalBeforeConfirm = new ArrayList<>();       //объём работы до подтверждения в журнале
    ArrayList<String> volumeInJournalAfterConfirm = new ArrayList<>();        //объём работы после подтверждения в журнале
    ArrayList<String> confirmedVolume = new ArrayList<>();                    //объём работы, передающийся из блокнота в журнал
    ArrayList<Integer> randomNumber = new ArrayList<>();                      //случайный номер чекбокса
    int numberOfRecords;                                                      //количество записей в блокноте

    @DisplayName("клик по работе с конкретным названием")
    public void selectMainWork (String workTitle) throws Exception {
        driver.findElement(By.xpath("//*[contains(text(),'" + workTitle + "')]")).click();
    }

    @DisplayName("перехожу к блокноту/журналу, считываю неподтверждённые работы и объёмы")
    public void openNotebook () throws Exception {
        builder.moveToElement(sidebar).click(workJournal).build().perform();
        builder.moveByOffset(1000, 0).perform();
        waitForElementPresence(confirmWork);
        driver.navigate().refresh();
        waitForElementPresence(confirmWork);
        workBeforeConfirm.clear();
        workAfterConfirm.clear();
        uncheckedWork.clear();
        unconfirmedVolume.clear();
        randomNumber.clear();
        Thread.sleep(2000);
        List<WebElement> list = driver.findElements(By.xpath("//div/span"));
        numberOfRecords=list.size();
        System.out.println("Количество записей в блокноте: " + numberOfRecords);
        for (int i=0;i<numberOfRecords;i++){
            workBeforeConfirm.add(driver.findElement(By.xpath("//div["+(i+1)+"]/div/span/../..//div[2]/p")).getText());
            unconfirmedVolume.add(driver.findElement(By.xpath("//div["+(i+1)+"]/div/span/../..//div[2]/div/p[2]")).getText());
            checkedWork.add(driver.findElement(By.xpath("//div["+(i+1)+"]/div/span/../..//div[2]/p")).getText());
            checkedVolume.add(driver.findElement(By.xpath("//div[" + (i+1) + "]/div/span/../..//div[2]/div/p[2]")).getText());
            randomNumber.add(i+1);
        }
    }

    @DisplayName("Отменяю подтверждение работ и проверяю соответствие значений")
    public void cancelConfirmWork () throws Exception {
        workAfterConfirm.clear();
        volumeInJournalBeforeConfirm.clear();
        volumeInJournalAfterConfirm.clear();
        for (int i=0;i<numberOfRecords;i++){
            List<WebElement> list2 = driver.findElements(By.xpath("//p[text()='" + getToday() + "']/../..//p[text()='"+workBeforeConfirm.get(i)+"']/../div/p[2]"));
            if (list2.size()==0){
                volumeInJournalBeforeConfirm.add("0");
            }
            else volumeInJournalBeforeConfirm.add(driver.findElement(By.xpath("//p[text()='" + getToday() + "']/../..//p[text()='"+workBeforeConfirm.get(i)+"']/../div/p[2]")).getText());
        }
        confirmWork.click();
        waitForElementPresence(cancelConfirmWorkButton);
        cancelConfirmWorkButton.click();
        Thread.sleep(2000);
        for (int i=0;i<numberOfRecords;i++){
            workAfterConfirm.add(driver.findElement(By.xpath("//div["+(i+1)+"]/div/span/../..//div[2]/p")).getText());
        }
        System.out.println("Работы в блокноте до подтверждения:           " + workBeforeConfirm);
        System.out.println("Работы в блокноте после отмены подтверждения: " + workAfterConfirm);
        for (int i=0;i<numberOfRecords;i++){
            List<WebElement> list2 = driver.findElements(By.xpath("//p[text()='" + getToday() + "']/../..//p[text()='"+workBeforeConfirm.get(i)+"']/../div/p[2]"));
            if (list2.size()==0){
                volumeInJournalAfterConfirm.add("0");
            }
            else volumeInJournalAfterConfirm.add(driver.findElement(By.xpath("//p[text()='" + getToday() + "']/../..//p[text()='"+workBeforeConfirm.get(i)+"']/../div/p[2]")).getText());
        }
        for (int i=0;i<numberOfRecords;i++){
            Assert.assertTrue(Double.parseDouble(volumeInJournalBeforeConfirm.get(i)) == Double.parseDouble(volumeInJournalAfterConfirm.get(i)));
        }
    }

    @DisplayName("Подтвердить выполнение отмеченных работ")
    public void confirmWork () throws Exception {
        workAfterConfirm.clear();
        volumeInJournalBeforeConfirm.clear();
        volumeInJournalAfterConfirm.clear();
        confirmedVolume.clear();
        System.out.println(checkedWork);
        Thread.sleep(2000);
        for (int i=0;i<checkedWork.size();i++){
            List<WebElement> list2 = driver.findElements(By.xpath("//p[text()='" + getToday() + "']/../..//p[text()='"+checkedWork.get(i)+"']"));
            if (list2.size()==0){
                volumeInJournalBeforeConfirm.add("0");
                confirmedVolume.add(Double.toString(Precision.round(Double.parseDouble(checkedVolume.get(i)), 6)));
            }
            else {
                volumeInJournalBeforeConfirm.add(driver.findElement(By.xpath("//p[text()='" + getToday() + "']/../..//p[text()='"+checkedWork.get(i)+"']/../div/p[2]")).getText());
                confirmedVolume.add(Double.toString(Precision.round(Double.parseDouble(checkedVolume.get(i)) + Double.parseDouble(volumeInJournalBeforeConfirm.get(i)), 6)));
            }
        }
        confirmWork.click();
        waitForElementPresence(applyConfirmWorksButton);
        applyConfirmWorksButton.click();
        Thread.sleep(2000);
        List<WebElement> list = driver.findElements(By.xpath("//div/span/span/input"));
        for (int i=0;i<list.size();i++){
            workAfterConfirm.add(driver.findElement(By.xpath("//div["+(i+1)+"]/div/span/../..//div[2]/p")).getText());
            //Assert.assertEquals(workAfterConfirm.get(i), uncheckedWork.get(i));
        }
        driver.navigate().refresh();
        for (int i=0;i<checkedWork.size();i++){
            volumeInJournalAfterConfirm.add(driver.findElement(By.xpath("//p[text()='" + getToday() + "']/../..//p[text()='"+checkedWork.get(i)+"']/../div/p[2]")).getText());
        }
        System.out.println("Не подтверждаем: " + uncheckedWork);
        System.out.println("   Подтверждаем: " + checkedWork);
        System.out.println("                   До: " + volumeInJournalBeforeConfirm);
        System.out.println("                 Плюс: " + checkedVolume);
        System.out.println("  Ожидаемый результат: " + confirmedVolume + " ");
        System.out.println("Фактический результат: " + volumeInJournalAfterConfirm);
        int countTrue=0;
        for (int i=0;i<confirmedVolume.size();i++){
            Assert.assertTrue(Double.parseDouble(confirmedVolume.get(i)) == Double.parseDouble(volumeInJournalAfterConfirm.get(i)));
            countTrue++;
        }
        if (countTrue==confirmedVolume.size()) {
            System.out.println("Корректные объёмы в журнале");
        }
    }

    @DisplayName("Проверяю соответствие записи в журнале и в детализации работы, системное сообщение о выполненном объёме")
    public void calculateConfirmVolume () throws Exception {
        var feed = new PageFeed(driver);
        Double sum=0.0;
        for (int i=0;i<checkedWork.size();i++) {
            List<WebElement> listConfirmVolume = driver.findElements(By.xpath("//*[contains(text(),'" + checkedWork.get(i) + "')]/../div/p[2]"));
            for (int j=0;j<listConfirmVolume.size();j++){
                sum=sum + Double.parseDouble(listConfirmVolume.get(j).getText());
            }
            checkedFullVolume.add(Double.toString(sum));
        }
    }

    @DisplayName("Проверяю соответствие записи в журнале и в детализации работы, системное сообщение о выполненном объёме")
    public void checkedSystemMessage () throws Exception {
        var feed = new PageFeed(driver);
        var work = new PageWorkProgress(driver);
        work.openProgressWork();
        Thread.sleep(2000);
        for (int i=0;i<checkedWork.size();i++) {
            waitForElementPresence(driver.findElement(By.xpath("//*[contains(text(),'" + checkedWork.get(i) + "')]")));
            driver.findElement(By.xpath("//*[contains(text(),'" + checkedWork.get(i) + "')]")).click();
            feed.viewSystemComplectedVolumeMessage(checkedVolume.get(i));
            Assert.assertEquals(checkedFullVolume.get(i), driver.findElement(By.xpath("//div[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div/div/div[2]/div[2]/div/div[2]/div/p")).getText());
            work.returnToListOfEstimates();
        }
    }

    @DisplayName("Клик на флажок конкретной записи")
    public void clickCheckboxWork (String workTitle) throws Exception {
        driver.findElement(By.xpath("//div/p[text()='" + workTitle + "']/../..//span")).click();
        if (uncheckedWork.size()==0)
            uncheckedWork.add(workTitle);
        else for (int i=0;i<uncheckedWork.size();i++)
            if (uncheckedWork.get(i).equals(workTitle))
                uncheckedWork.remove(i);
    }

    @DisplayName("Клик на флажок случайной записи")
    public void clickRandomCheckboxWork() throws Exception {
        int rnd = (int) (Math.random() * (randomNumber.size()));
        driver.findElement(By.xpath("//div["+(randomNumber.get(rnd))+"]/div/span")).click();
        checkedWork.add(driver.findElement(By.xpath("//div["+(randomNumber.get(rnd))+"]/div/span/../..//div[2]/p")).getText());
        checkedVolume.add(driver.findElement(By.xpath("//div[" + (randomNumber.get(rnd)) + "]/div/span/../..//div[2]/div/p[2]")).getText());
        System.out.println("Номер записи: " + randomNumber.get(rnd));
        randomNumber.remove(rnd);
        uncheckedWork.remove(rnd);
    }


    @DisplayName("Снять все флажки")
    public void uncheckAllWorks () throws Exception {
        workBeforeConfirm.clear();
        unconfirmedVolume.clear();
        List<WebElement> list = driver.findElements(By.xpath("//div/span"));
        for (int i=0;i<list.size();i++){
            checkedWork.remove(list.size()-i-1);
            checkedVolume.remove(list.size()-i-1);
            list.get(i).click();
            uncheckedWork.add(driver.findElement(By.xpath("//div["+(i+1)+"]/div/span/../..//div[2]/p")).getText());
        }
    }

    @DisplayName("Очистка блокнота")
    public void cleanNotebook () throws Exception {
        openNotebook();
        waitForElementPresence(driver.findElement(By.xpath("//*[contains(text(),'Подтвердить выполнение')]")));
        List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'Подтвердить выполнение')]"));
        if (list.size() > 0 ) {
            confirmWork();
        }
        Thread.sleep(2000);
    }

    @DisplayName("получаю сегодняшнюю дату")
    public String getToday() throws Exception {
        String todayMonth = "";
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        String dayOfWeek = Integer.toString(cal.get(Calendar.DAY_OF_WEEK));
        String todayDay = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
        switch (month) {
            case 0:
                todayMonth = "января";
                break;
            case 1:
                todayMonth = "февраля";
                break;
            case 2:
                todayMonth = "марта";
                break;
            case 3:
                todayMonth = "апреля";
                break;
            case 4:
                todayMonth = "мая";
                break;
            case 5:
                todayMonth = "июня";
                break;
            case 6:
                todayMonth = "июля";
                break;
            case 7:
                todayMonth = "августа";
                break;
            case 8:
                todayMonth = "сентября";
                break;
            case 9:
                todayMonth = "октября";
                break;
            case 10:
                todayMonth = "ноября";
                break;
            case 11:
                todayMonth = "декабря";
                break;
        }
        switch (dayOfWeek) {
            case "1":
                dayOfWeek = "воскресенье";
                break;
            case "2":
                dayOfWeek = "понедельник";
                break;
            case "3":
                dayOfWeek = "вторник";
                break;
            case "4":
                dayOfWeek = "среда";
                break;
            case "5":
                dayOfWeek = "четверг";
                break;
            case "6":
                dayOfWeek = "пятница";
                break;
            case "7":
                dayOfWeek = "суббота";
                break;
        }
        String today = todayDay.concat(" ").concat(todayMonth).concat(", ").concat(dayOfWeek);
        return today;
    }

    @DisplayName("получаю сегодняшнюю дату")
    public String getTodayWithYear() throws Exception {
        String todayMonth = "";
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        String todayDay = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
        switch (month) {
            case 0:
                todayMonth = "января";
                break;
            case 1:
                todayMonth = "февраля";
                break;
            case 2:
                todayMonth = "марта";
                break;
            case 3:
                todayMonth = "апреля";
                break;
            case 4:
                todayMonth = "мая";
                break;
            case 5:
                todayMonth = "июня";
                break;
            case 6:
                todayMonth = "июля";
                break;
            case 7:
                todayMonth = "августа";
                break;
            case 8:
                todayMonth = "сентября";
                break;
            case 9:
                todayMonth = "октября";
                break;
            case 10:
                todayMonth = "ноября";
                break;
            case 11:
                todayMonth = "декабря";
                break;
        }
        String todayWithYear = todayDay.concat(" ").concat(todayMonth).concat(" ").concat(Integer.toString(year));
        return todayWithYear;
    }

    @DisplayName("Проверка записи в блокноте")
    public void checkRecordWorkInNotebook (String estimateTitle, String workTitle, String volume){
        List<WebElement> list = driver.findElements(By.xpath("//p[text()='" + estimateTitle + "']/../..//p[text()='" + workTitle + "']"));
        Assert.assertTrue(list.size() == 1);
        String sendVolume = driver.findElement(By.xpath("//p[text()='" + estimateTitle + "']/../..//p[text()='" + workTitle + "']/../div/p[2]")).getText();
        Assert.assertEquals(volume.concat(" "), sendVolume);
    }

    @DisplayName("Для проверки")
    public void check() throws Exception {

    }

}