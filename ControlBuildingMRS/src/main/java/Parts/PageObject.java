package Parts;

import Helpers.BaseTest;
import jdk.jfr.Label;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;



public class PageObject extends BaseTest {

    public PageObject (WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    Actions builder = new Actions(driver);

    /**PageFeed */
    //@FindBy(xpath = "//*[@data-ui-test='workCommentField']")
    @FindBy(xpath = "//div[3]/div/div/div/input")
    public WebElement workCommentField;

    @FindBy(xpath = "//*[@data-ui-test='workAddFileToComment']")
    public WebElement workAddFileToComment;

    //@FindBy(xpath = "//*[@data-ui-test='workSendComment']")
    @FindBy(xpath = "//div[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[3]/div/div[3]/div/button")
    public WebElement workSendComment;

    /**PageNotebook */
    //@FindBy(xpath = "//*[@data-ui-test='workJournal']")
    //@FindBy(xpath = "//a[4]/span/div")
    @FindBy(xpath = "//p[contains(.,'Журнал')]")
    public WebElement workJournal;

    //@FindBy(xpath = "//*[@data-ui-test='returnToListOfEstimates']")
    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div[1]/div/button")
    public WebElement returnToListOfEstimatesButton;

    //@FindBy(xpath = "//*[@data-ui-test='checkBoxNotebook']")
    @FindBy(xpath = "//div[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div[3]/div/div/span/span/input")
    public WebElement checkBoxNotebook;

    //@FindBy(xpath = "//*[@data-ui-test='confirmWork']")
    @FindBy(xpath = "//div[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div[4]/button")
    public WebElement confirmWork;

    //@FindBy(xpath = "//*[@data-ui-test='applyConfirmWorks']")
    @FindBy(xpath = "//button[contains(.,'Да')]")
    public WebElement applyConfirmWorksButton;

    //@FindBy(xpath = "//*[@data-ui-test='cancelConfirmWork']")
    @FindBy(xpath = "//div[3]/div/div/div/button")
    public WebElement cancelConfirmWorkButton;

    /**PageProject */
    //@FindBy(xpath = "//*[@data-ui-test='myPage']")
    @FindBy(xpath = "//*[@data-ui-test='myPage']")
    public WebElement myPage;

    //@FindBy(xpath = "//*[@data-ui-test='projectPage']")
    @FindBy(xpath = "//*[@id='app']/div/div/div/div/div/ul/div[2]/div")
    public WebElement projectSidebar;

    //@FindBy(xpath = "//*[@data-ui-test='createProjectButton']")
    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div[1]/button")
    public WebElement createProjectButton;

    @FindBy(xpath = "//*[@data-ui-test='createNewProjectAtMyPage']")
    public WebElement createProjectAtMyPageButton;

    //@FindBy(xpath = "//*[@data-ui-test='createNewProjectAtSideBar']")
    @FindBy(xpath = "//div[@id='app']/div/div/div/div/div/ul/div[2]/button")
    public WebElement createProjectAtSideBarButton;

    @FindBy(xpath = "//*[@data-ui-test='myProject']")
    public WebElement myProject;

    @FindBy(xpath = "//*[@data-ui-test='createNewProjectAtDropDawnList']")
    public WebElement createProjectAtDropDawnListButton;

    //@FindBy(xpath = "//*[@data-ui-test='cancelCreateProject']")
    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[1]")
    public WebElement cancelCreateProjectButton;

    //@FindBy(xpath = "//*[@data-ui-test='confirmCreateProject']")
    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[2]")
    public WebElement confirmCreateProjectButton;

    //@FindBy(xpath = "//*[@data-ui-test='nameProject']")
    @FindBy(xpath = "//div[3]/div/div[2]/div/div[2]/div/div/input")
    public WebElement nameProject;

    //@FindBy(xpath = "//*[@data-ui-test='buildingAddress']")
    @FindBy(xpath = "//div[2]/div[2]/div/input")
    public WebElement addressProject;

    @FindBy(xpath = "//*[@data-ui-test='loadPhoto']")
    public WebElement loadPhoto;

    @FindBy(xpath = "//*[@id='fade-menu']/div[3]/div/div/li[1]")
    public WebElement editProjectButton;

    @FindBy(xpath = "//*[@id='fade-menu']/div[3]/div/div/li[2]")
    public WebElement addParticipantsToProjectButton;

    @FindBy(xpath = "//*[@id='fade-menu']/div[3]/div/div/li[3]")
    public WebElement deleteProjectButton;

    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[1]")
    public WebElement cancelEditProjectButton;

    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[2]")
    public WebElement saveEditProjectButton;


    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div[1]/div/button")
    public WebElement addParticipantToProjectButtonInParticipants;

    @FindBy(xpath = "//div[2]/div[3]/div/div[2]/div/div[1]/div/div/div")
    public WebElement chooseOrganization;

    @FindBy(xpath = "//div[2]/div[3]/div/div[2]/div/div[2]/div/div/div/div[1]")
    public WebElement chooseTypeOrganization;

    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[1]")
    public WebElement cancelAddOrganization;

    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[2]")
    public WebElement confirmAddOrganization;

    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[1]")
    public WebElement cancelAddEmployeeButton;

    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[2]")
    public WebElement confirmAddEmployeeButton;


    /**PageWorkProgress */
    //@FindBy(xpath = "//*[@data-ui-test='workProgress']")
    @FindBy(xpath = "//p[contains(.,'Прогресс работ')]")
    public WebElement workProgressSidebar;

    @FindBy(xpath = "//div[@id='app']/div/div/div/div/div/ul")
    public WebElement sidebar;

    //@FindBy(xpath = "//*[@data-ui-test='worksSearchField']")
    @FindBy(xpath = "//div[@id='app']/div/div[2]/div/div[2]/div/div/div/div/div/div/div/input")
    public WebElement worksSearchField;

    //@FindBy(xpath = "//*[@data-ui-test='mainWorksTab']")
    @FindBy(xpath = "div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div/div/div/p")
    public WebElement mainWorksTab;

    //@FindBy(xpath = "//*[@data-ui-test='additionalWorksTab']")
    @FindBy(xpath = "//div[@id='app']/div/main/div/div/div[2]/div[2]/div/div[2]/div/div/div/div/button[2]")
    public WebElement additionalWorksTab;

    @FindBy(xpath = "//p[contains(.,'ПОКРЫТИЕ ИЗ ПРОФИЛИРОВАННОГО ЛИСТА')]")
    public WebElement EstimateTitle;

    //@FindBy(xpath = "//*[@data-ui-test='addingWorkRecord']")
    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div/div[2]/button")
    public WebElement addingWorkRecord;

    @FindBy(xpath = "//*[@data-ui-test='cancelAddingRecord']")
    public WebElement cancelAddingRecord;

    @FindBy(xpath = "//*[@data-ui-test='uploadFileToRecord']")
    public WebElement uploadFileToRecord;

    //@FindBy(xpath = "//*[@data-ui-test='VolumeWork']")
    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div[1]/div[1]/div/input")
    public WebElement enterVolumeWork;

    @FindBy(xpath = "//div[3]/div/div[2]/div/div/div[2]/div")
    public WebElement messagesUnit;

    //@FindBy(xpath = "//*[@data-ui-test='sendToNotebook']")
    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div[3]/div/button[2]")
    public WebElement sendToNotebook;

    //@FindBy(xpath = "//*[@data-ui-test='sendToNotebook']")
    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[1]")
    public WebElement sendToNotebookWithoutPhoto;

    //@FindBy(xpath = "//*[@data-ui-test='sendToNotebook']")
    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[2]")
    public WebElement notSendToNotebookWithoutPhoto;

    //@FindBy(xpath = "//*[@data-ui-test='createWorkBtn']")
    @FindBy(xpath = "//button[contains(.,'Создать работу')]")
    public WebElement createWorkBtn;

    @FindBy(xpath = "//input[@name='title']")
    public WebElement titleNewWork;

    @FindBy(xpath = "//input[@name='valueTotal']")
    public WebElement valueNewWork;

    @FindBy(xpath = "//div[3]/div/div[2]/div/div[2]/div[2]/div/div")
    public WebElement unitsNewWork;

    @FindBy(xpath = "//input[@name='cost']")
    public WebElement costNewWork;

    @FindBy(xpath = "//input[@name='start']")
    public WebElement startDateNewWork;

    @FindBy(xpath = "//input[@name='finish']")
    public WebElement finishDateNewWork;

    //@FindBy(xpath = "//*[@data-ui-test='applyAddWork']")
    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[2]")
    public WebElement applyAddNewWork;

    //@FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[1]")
    @FindBy(xpath = "//button[contains(.,'Отмена')]")
    public WebElement cancelAddNewWork;

    @FindBy(xpath = "//*[@data-ui-test='additionalWorkComment']")
    public WebElement additionalWorkComment;

    /**PageArea */
    //@FindBy(xpath = "//*[@data-ui-test='area']")
    @FindBy(xpath = "//p[contains(.,'Участки')]")
    public WebElement area;

    //@FindBy(xpath = "//*[@data-ui-test='addObjectButton']")
    @FindBy(xpath = "//div[@id='app']/div/main/div/div/div/div/button")
    public WebElement addObjectButton;

    //@FindBy(xpath = "//*[@data-ui-test='actionsListButton']")
    @FindBy(xpath = "//div[@id='app']/div/main/div/div/div[2]/div/div/div/div/button")
    public WebElement actionsListButton;

    //@FindBy(xpath = "//*[@data-ui-test='editTheObjectButton']")
    @FindBy(xpath = "//div[@id='fade-menu']/div[3]/div/div/div[1]")
    public WebElement editTheObjectButton;

    //@FindBy(xpath = "//*[@data-ui-test='createAreasButton']")
    //@FindBy(xpath = "//*[@id='fade-menu']/div[3]/div/div/div[2]")
    @FindBy(xpath = "//*[contains(text(),'Создать участок')]")
    public WebElement createAreasButton;

    @FindBy(xpath = "//input[@value='']")
    public WebElement titleArea;

    //@FindBy(xpath = "//*[@data-ui-test='deleteTheObjectButton']")
    @FindBy(xpath = "//*[@id='fade-menu']/div[3]/div/div/div[3]")
    public WebElement deleteTheObjectButton;

    //@FindBy(xpath = "//*[@data-ui-test='applyEditButton']")
    @FindBy(xpath = "//button[2]")
    public WebElement applyEditButton;

    @FindBy(xpath = "//*[@data-ui-test='typeObjectDropDawnList']")
    public WebElement typeObjectDropDawnList;

    //@FindBy(xpath = "//*[@data-ui-test='nameObject']")
    @FindBy(xpath = "//input[@name='title']")
    public WebElement nameObject;

    //@FindBy(xpath = "//*[@data-ui-test='addressObject']")
    @FindBy(xpath = "//input[@name='address']")
    public WebElement addressObject;

    //@FindBy(xpath = "//*[@data-ui-test='cancelButton']")
    @FindBy(xpath = "//button[contains(.,'Отмена')]")
    public WebElement cancelButton;

    //@FindBy(xpath = "//*[@data-ui-test='createObjectButton']")
    @FindBy(xpath = "//button[contains(.,'Создать объект')]")
    public WebElement createObjectButton;

    @FindBy(xpath = "//*[@data-ui-test='areaTitleField']")
    public WebElement areaTitleField;

    @FindBy(xpath = "//*[@data-ui-test='areaNumberField']")
    public WebElement areaNumberField;


    /**PagePSD */

    //@FindBy(xpath = "//*[@data-ui-test='PSDPage']")
    @FindBy(xpath = "//div[@id='app']/div/div/div/div/div/ul/div[3]/div[1]/li[2]")
    public WebElement PSDsidebar;

    //@FindBy(xpath = "//*[@data-ui-test='createFolderInPSD']")
    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div[2]/div[1]/div/div[1]/button")
    public WebElement createFolderInPSD;

    //@FindBy(xpath = "//*[@data-ui-test='nameFolderField']")
    @FindBy(xpath = "//div[@id='app']/div/main/div/div/div[2]/div/div/div[2]/div/div/div/div/div/input")
    public WebElement nameFolderField;

    //@FindBy(xpath = "//*[@data-ui-test='editFolderInPSD']")
    @FindBy(xpath = "//div[2]/div[3]/div/li/p")
    public WebElement editFolderInPSD;

    //@FindBy(xpath = "//*[@data-ui-test='addFolderInPSD']")
    @FindBy(xpath = "//div[2]/div[3]/div/li[2]")
    public WebElement addFolderInPSD;

    //@FindBy(xpath = "//*[@data-ui-test='deleteFolderInPSD']")
    @FindBy(xpath = "//div[2]/div[3]/div/li[3]")
    public WebElement deleteFolderInPSD;

    //@FindBy(xpath = "//*[@data-ui-test='loadFileButton']")
    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div[1]/div[2]/section/div")
    public WebElement loadFileButton;

    //@FindBy(xpath = "//*[@data-ui-test='loadFileField']")
    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div/div[2]/section/div/input")
    public WebElement loadFileField;

    //@FindBy(xpath = "//*[@data-ui-test='importFilesButton']")
    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[3]/div/div/div/div/button[1]")
    public WebElement importFilesButton;

    //@FindBy(xpath = "//*[@data-ui-test='goToProgressWorkNo']")
    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[1]")
    public WebElement goToProgressWorkNo;

    //@FindBy(xpath = "//*[@data-ui-test='goToProgressWorkYes']")
    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[2]")
    public WebElement goToProgressWorkYes;

    //@FindBy(xpath = "//*[@data-ui-test='deleteFilesButton']")
    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div[2]/div[2]/div/div[3]/div/div/div/div/button[2]")
    public WebElement deleteFilesButton;


    @Label("Ждет, когда элемент появится на странице")
    public void waitForElementPresence(WebElement element) {
        new WebDriverWait(driver, driverWait)
                .until(ExpectedConditions.visibilityOf(element));
    }

    @Label("Ждет, что элемента на странице не будет")
    public void waitForElementNotPresented(By Xpath) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        new WebDriverWait(driver, driverWait)
                .until(ExpectedConditions.numberOfElementsToBe(Xpath, 0));
        driver.manage().timeouts().implicitlyWait(driverWait, TimeUnit.SECONDS);
    }

    @Label("Выделяет весь текст и удаяет")
    public static String deleteString = Keys.chord(Keys.CONTROL, "a") + Keys.DELETE;

    @Label("Wait for element is presented in a page. Option 2")
    public WebElement getWhenVisible(By locator) {
        WebElement element = null;
        WebDriverWait wait = new WebDriverWait(driver, driverWait);
        element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return element;
    }
}
