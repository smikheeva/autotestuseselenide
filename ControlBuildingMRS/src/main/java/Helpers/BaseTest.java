package Helpers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;


public class BaseTest {
    protected static WebDriver driver;
    //protected static String projectUrl = "https://mrs-fieldlog-app.s3.plotpad.com";
    //protected static String projectUrl = "https://mrs-general-app.s3.plotpad.com";
    protected static String projectUrl = "https://staging.mp.mrsdev.ru";
    protected int driverWait = 20;

    @BeforeTest
    @Parameters("browser")
    public void openBrowser(String browser){

        if (browser.equalsIgnoreCase("Chrome")) {
            driver = new ChromeDriver();
        }
        else if (browser.equalsIgnoreCase("Opera")) {
            driver = new OperaDriver();
        }
        else if (browser.equalsIgnoreCase("Firefox")) {
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions options = new FirefoxOptions();
            options.setBinary("/usr/share/applications/firefox");
            driver = new FirefoxDriver(options);
        }
        else if (browser.equalsIgnoreCase("Yandex")) {
            WebDriverManager.operadriver().setup();
            OperaOptions options = new OperaOptions();
            options.setBinary("/usr/bin/yandex-browser-beta");
            driver = new OperaDriver(options);
        }
    }

    public static void startTest(){
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        driver.get(projectUrl);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @AfterTest
    public static void closeTest() {
        //driver.quit();
        //driver.navigate().refresh();
    }
}
