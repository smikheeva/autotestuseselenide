package Helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Assistants extends BaseTest{

    public static WebDriver driver;


    public static boolean isTextPresent(String text){
        try{
            boolean b = driver.getPageSource().contains(text);
            return b;
        }
        catch(Exception e){
            return false;
        }
    }

    public static boolean isElementPresent(By locatorKey) {
        try {
            driver.findElement(locatorKey);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }




}
