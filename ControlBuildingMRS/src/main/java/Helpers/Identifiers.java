package Helpers;

public class Identifiers {

    public int waitTime = 1000;


    /**Боковое меню*/
    public String myObject                        = "//*[@data-ui-test='myObject']";                                        //мой объект
    public String workProgress                    = "//*[@data-ui-test='workProgress']";                                    //Прогресс работ
    public String estimateDocumentation           = "//*[@data-ui-test='estimateDocumentation']";                           //ПСД
    public String executiveDocumentation          = "//*[@data-ui-test='executiveDocumentation']";                          //Исполнительная документация
    public String workJournal                     = "//*[@data-ui-test='workJournal']";                                     //Журнал
    public String myPage                          = "//*[@data-ui-test='myPage']";                                          //моя страница
    public String myProject                       = "//*[@data-ui-test='myProject']";                                       //мой проект
    public String createNewProjectAtLeftPanel     = "//*[@data-ui-test='createNewProjectAtLeftPanel']";                     //создать проект, если еще ни один проект не создан
    public String createNewProjectAtDropDawnList  = "//*[@data-ui-test='createNewProjectAtLeftPanel']";                     //создать проект из выпадающего списка


    /** Форма создания проекта*/
    public String nameProject                            = "//*[@data-ui-test='nameProjectField']";                         //поле ввода имени проекта
    public String buildingAddress                        = "//*[@data-ui-test='addressProjectField']";                      //поле ввода строительного адреса
    public String loadPhoto                              = "//*[@data-ui-test='loadPhotoProject']";                         //кнопка "загрузить фотографию"
    public String confirmCreateProject                   = "//*[@data-ui-test='confirmCreateProject']";                     //кнопка "Создать проект"
    public String cancelCreateProject                    = "//*[@data-ui-test='cancelCreatingProjectButton']";              //кнопка "Отменить"

    /**Моя страница*/
    public String createNewProjectAtMyPage               = "//*[@data-ui-test='buttonCreateNewProjectAtMyPage']";           //кнопка "Создать проект" на моей странице

    /**Прогресс работ*/
    public String worksSearchField              = "//*[@data-ui-test='worksSearchField']//input";                           //+строка поиска

    /**Сметы*/
    public String testEstimateTitle             = "//p[contains(.,'ПОКРЫТИЕ ИЗ ПРОФИЛИРОВАННОГО ЛИСТА')]";
    public String mainWorksTab                  = "//*[@data-ui-test='mainWorksTab']";                                      //+основные работы вкладка
    public String additionalWorksTab            = ".MuiButtonBase-root-261:nth-child(2) .MuiTab-label-406";                 //+дополнительные работы
    public String estimatedStartDate            = "//*[@data-ui-test='estimatedStartDate']";                                //дата начала по смете
    public String estimateEndDate               = "//*[@data-ui-test='estimateEndDate']";                                   //дата окончания по смете
    public String responsibleForEstimates       = "//*[@data-ui-test='responsibleForEstimates']";                           //Поле Ответсвенный
    public String nameOfResponsibleForEstimate  = "//*[@data-ui-test='nameOfResponsibleForEstimate']";                       //Конкретный ответственный

    /**Работы*/
    public String addingWorkRecord      = "";
    public String mainWorkTitle         = "//div/p[text()='Перевозка грузов I класса автомобилями-самосвалами грузоподъемностью 10 т работающих вне карьера на расстояние: до 3 км']";
    public String enterVolumeWork       = ".inputFieldText__2ENnG";                                     //поле "Введите объем"
    public String returnToListOfWorks   = "//*[@data-ui-test='returnToListOfWorks']";                   //возврат к списку работ return to the list of works
    public String sendToNotebook        = "";                           //Отправить в блокнот
    public String sendToNotebookWithoutPhoto       = "";                           //Отправить в блокнот без фото
    public String workCommentField      = "//*[@data-ui-test='workCommentField']";                      //Поле ввода комментарий
    public String uploadFileToRecord = "//*[@data-ui-test='uploadFileToRecord']";     //загрузка файла к записи о выполнении работ
    public String testMainWorkTitle     = "тут будет локатор";                                          //Работа с конкретным именем

    /**Дополнительная работа. Создание*/
    public String createWorkBtn         = ".MuiButton-label-412";                       //+Кнопка создать новую работу
    //??? кнопка создать работу,когда уже есть работы
    public String additionalWorkName    = "body > div.MuiModal-root-245.MuiDialog-root-247 > div.MuiDialog-container-250.MuiDialog-scrollPaper-248 > div > div.addExtraWorkModalContainer__32UqI > div.MuiFormControl-root-788 > div > input";                    //поле название доп работы
    public String addWorkVolume         = "body > div.MuiModal-root-245.MuiDialog-root-247 > div.MuiDialog-container-250.MuiDialog-scrollPaper-248 > div > div.addExtraWorkModalContainer__32UqI > div:nth-child(3) > div.value__2dhLw > div > div > input";                         //поле Необходимый объем
    public String addWorkComment        = "//*[@data-ui-test='addWorkComment']";                        //поле Комментарий к доп работе
    public String measureAddWorkField   = "#select-workMeasure";                   //поле ед. измерения
    public String testMeasureAddWork    = "//li[contains(.,'км')]";                                      //конкретная ед. измерения
    public String startAddWorkDate      = "//*[@data-ui-test='startAddWorkDate']";                      //дата начала дополнительной работы
    public String cancelAddWork         = "//*[@data-ui-test='cancelAddWork']";                         //кнопка Отмена создания доп работы
    public String applyAddWork          = "body > div.MuiModal-root-245.MuiDialog-root-247 > div.MuiDialog-container-250.MuiDialog-scrollPaper-248 > div > div.addExtraWorkModalContainer__32UqI > div.btnContainer__2FsMF > button.MuiButtonBase-root-261.MuiButton-root-411.MuiButton-contained-422.MuiButton-containedSecondary-424.MuiButton-raised-425.MuiButton-raisedSecondary-427";                          //кнопка Да, создать доп работу



    /**Журнал/Блокнот*/
    public String confirmTodayWork                = "//*[@data-ui-test='confirmTodayWork']";                      //кнопка Подтвердить выполнение
    public String applyConfirmWorks               = "//*[@data-ui-test='applyConfirmWorks']";                     //согласие с подтверждением
    public String cancelConfirmingTodayWork       = "//*[@data-ui-test='cancelConfirmingTodayWork']";             //кнопка Отменить подтверждение выполнения
    public String todayWorkSelection              = "//*[@data-ui-test='todayWorkSelection']";                   //чек-бокс выделения работы в блокноте  work selection



    /**Комментарии*/
    public String workAddFileToComment            = "//*[@data-ui-test='workAddFileToComment']";                      //кнопка прикрепить файл к комментарию
    public String workSendComment                 = "//*[@data-ui-test='workSendComment']";                           //кнопка отправить комментарий
    public String workCommentFieldInHistory       = "//*[@data-ui-test='workCommentFieldInHistory']";                 //поле ввода комментария в истории



}
