import Helpers.BaseTest;
import Parts.PageArea;
import Parts.PageNotebook;
import Parts.PageWorkProgress;
import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;

public class NotebookTest extends BaseTest {

    private String testUserEmail     = "test@ya.ru";
    private String testUserPassword  = "Qwerty123";
    private String estimate1         = "Благоустройство и озеленение территории";
    private String mainWork11        = "Разборка надземной части без сохранения годных материалов: кирпичных зданий 1, 2-этажных";
    private String valueWork11       = Double.toString(Precision.round((Math.random()+0.1)/10, 4));
    private String mainWork12        = "Погрузочные работы при автомобильных перевозках: мусора строительного с погрузкой экскаваторами емкостью ковша до 0,5 м3";
    private String valueWork12       = Double.toString(Precision.round((Math.random()+0.1)/10, 4));
    private String estimate2         = "ДВЕРИ, ОКНА, ВИТРАЖИ";
    private String mainWork21        = "Установка металлических дверных блоков в готовые проемы";
    private String valueWork21       = Double.toString(Precision.round((Math.random()+0.1)/10, 4));

        /*note.openNotebook();
        note.cancelConfirmWork();
        note.uncheckAllWorks();
        note.clickRandomCheckboxWork();
        note.clickRandomCheckboxWork();
        note.confirmWork();*/



    @DisplayName("Отмена подтверждения работ")
    @Test
    public void cancelConfirmWork() throws Exception {
        var note = new PageNotebook(driver);
        note.openNotebook();
        note.cancelConfirmWork();
    }

    @DisplayName("Подтверждение 2 работ")
    @Test
    public void someConfirmWork() throws Exception {
        var note = new PageNotebook(driver);
        note.uncheckAllWorks();
        note.clickRandomCheckboxWork();
        note.clickRandomCheckboxWork();
        note.confirmWork();
    }

    @DisplayName("Подтверждение всех работ")
    @Test
    public void allConfirmWork() throws Exception {
        var note = new PageNotebook(driver);
        note.confirmWork();
    }



    @DisplayName("проверяю соответствие объема работы")
    @Test
    public void Check() throws Exception {
        startTest();
        var work = new PageWorkProgress(driver);
        var note = new PageNotebook(driver);
        var area = new PageArea(driver);
        area.openAreaSideBar();
        area.cancelAddingObjectButton();
        Thread.sleep(2000);
        note.openNotebook();
        //note.cleanNotebook();
        work.openProgressWork();
        Thread.sleep(2000);
        work.selectRandomEstimate();
        work.confirmAddingRecordOnWork();
        Thread.sleep(2000);
        note.openNotebook();
        Thread.sleep(2000);
        note.confirmWork();
        Thread.sleep(2000);
        driver.navigate().refresh();
        Thread.sleep(4000);
    }
}