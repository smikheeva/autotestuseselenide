import Helpers.BaseTest;
import Parts.PageFeed;
import Parts.PageNotebook;
import Parts.PageWorkProgress;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;

public class FeedTest extends BaseTest {

    private String testUserEmail = "test@ya.ru";
    private String testUserPassword = "Qwerty123";

    private String estimate     = "Благоустройство и озеленение территории";
    private String mainWork     = "Разборка надземной части без сохранения годных материалов: кирпичных зданий 1, 2-этажных";
    private String myComment    = "Это мой комментарий к работе";
    private String fileName     = "/home/user/Изображения/Строительные работы/little_jpg.jpg";
    private String volumeWork   = "1.5";
    private String titleNewWork = "5 тестовая дополнительная работа";

    @DisplayName("Отправить текстовый комментарий")
    @Test(priority=1)
    public void SendingTextComment() throws Exception {
        startTest();
        var feed = new PageFeed(driver);
        var work = new PageWorkProgress(driver);
        work.openProgressWork();
        work.selectRandomEstimateAndWork();
        feed.sendTextComment(myComment);
        feed.checkComment(myComment);
    }

    @DisplayName ("Проверить системное сообщение о создании дополнительной работы")
    @Test(priority=2)
    public void SystemMessageAboutNewWork() throws Exception {
        startTest();
        var feed = new PageFeed(driver);
        var work = new PageWorkProgress(driver);
        work.openProgressWork();
        work.selectRandomEstimate();
        work.confirmCreatingNewWorkWithoutComment(titleNewWork);
        feed.viewSystemMessageAboutNewWork(titleNewWork);
    }

    @DisplayName ("Проверить системное сообщение о подтверждении выполнения объёма работы")
    @Test(priority=3)
    public void SystemMessageAboutConfirmWork() throws Exception {
        startTest();
        var note = new PageNotebook(driver);
        var feed = new PageFeed(driver);
        var work = new PageWorkProgress(driver);
        work.openProgressWork();
        work.selectRandomEstimateAndWork();
        work.confirmAddingRecordOnWork();
        note.openNotebook();
        note.confirmWork();
        work.openProgressWork();
        feed.viewSystemComplectedVolumeMessage(volumeWork);
    }


    @Test
    public void check() throws Exception {
        startTest();
        var note = new PageNotebook(driver);
        var work = new PageWorkProgress(driver);
        work.openProgressWork();
        work.selectRandomEstimateAndWork();
        work.confirmAddingRecordOnWork();
        note.openNotebook();
        note.confirmWork();
        note.checkedSystemMessage();
    }

}