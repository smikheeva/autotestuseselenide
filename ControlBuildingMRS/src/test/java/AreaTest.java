import Helpers.BaseTest;
import Parts.PageArea;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;

public class AreaTest extends BaseTest {
    private String testUserEmail     = "test@ya.ru";
    private String testUserPassword  = "Qwerty123";

    private String titleObject = "Новый объект";
    private String addressObject = "Адрес нового объект";
    private String titleArea = "Участок нового объекта";

    @DisplayName("Создать новый объект")
    @Test
    public void CreatingObjectNewBuilding() throws InterruptedException {
        startTest();
        var area = new PageArea(driver);
        area.openAreaSideBar();
        area.addObjectNewBuilding(titleObject, addressObject);
    }

    @DisplayName("Создать новый объект и несколько участков к нему")
    @Test
    public void CreateManyAreas() throws Exception {
        startTest();
        var area = new PageArea(driver);
        area.openAreaSideBar();
        Thread.sleep(4000);
        area.addObjectNewBuilding(titleObject, addressObject);
        Thread.sleep(2000);
        area.createManyAreas(titleObject, 50);
    }
}
