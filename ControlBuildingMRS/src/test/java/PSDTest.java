import Helpers.BaseTest;
import Parts.PageFeed;
import Parts.PageProject;
import Parts.PagePsd;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;

public class PSDTest extends BaseTest {
    private String testUserEmail = "test@ya.ru";
    private String testUserPassword = "Qwerty123";
    private String testUserPassword2 = "123456";
    private String addFolder = ""+Math.random();
    private String estimateLink1 = "/home/user/Документы/QA/Сметы/Arp/Шкаф IP54.arp";
    private String estimateLink2 = "/home/user/Документы/QA/Сметы/Arp/Здание автомойки.arp";
    private String estimateLink3 = "/home/user/Документы/QA/Сметы/Arp/Котельная 2,6 МВТ д. Борисово (Лента).arp";
    private String estimateLink4 = "/home/user/Документы/QA/Сметы/Arp/Общестроительные работы. Надземная часть.arp";
    private String estimateLink5 = "/home/user/Документы/QA/Сметы/Arp/Благоустройство и озеленение территории.arp";
    private String estimateLink6 = "/home/user/Документы/QA/Сметы/Arp/СТЕНЫ ПОДЗЕМНОЙ ЧАСТИ.arp";
    private String estimateLink7 = "/home/user/Документы/QA/Сметы/Arp/ЗЕМЛЯНЫЕ РАБОТЫ.arp";
    private String estimateLink8 = "/home/user/Документы/QA/Сметы/Arp/СВАЙНОЕ ПОЛЕ И МОНОЛИТНЫЕ ФУНДАМЕНТЫ.arp";
    private Integer numberOfCreatedFolders = 2;  //сколько папок создать

    @DisplayName("Открыть проектно-сметную документацию")
    @Test
    public void openPSD() throws Exception {
        //startTest();
        var psd = new PagePsd(driver);
        var feed = new PageFeed(driver);
        var project = new PageProject(driver);
        //project.openProjectPage();
        project.chooseProject("123");
        Thread.sleep(8000);
        psd.openPSD();
    }

    @DisplayName("Создать папки в количестве numberOfCreatedFolders")
    @Test
    public void createFolders() throws Exception {
        var psd = new PagePsd(driver);
        psd.createNewFolder(numberOfCreatedFolders);
    }

    @DisplayName("Редактировать папку")
    @Test
    public void editFolder() throws Exception {
        var psd = new PagePsd(driver);
        psd.editRandomFolder();
    }

    @DisplayName("Удалить папку")
    @Test
    public void deleteFolder() throws Exception {
        var psd = new PagePsd(driver);
        psd.deleteRandomFolder();
    }

    @DisplayName("Добавить папку в папку")
    @Test
    public void addFolderToFolder() throws Exception {
        var psd = new PagePsd(driver);
        psd.addFolderToFolder(addFolder);
    }

    @DisplayName("Загрузить файл в папку")
    @Test
    public void loadFileToFolder() throws Exception {
        var psd = new PagePsd(driver);
        psd.selectRandomFolder();
        psd.loadFile(estimateLink1);
        driver.navigate().refresh();
        psd.loadFile(estimateLink2);
        driver.navigate().refresh();
        psd.loadFile(estimateLink3);
        driver.navigate().refresh();
        psd.loadFile(estimateLink4);
        psd.loadFile(estimateLink5);
    }

    @DisplayName("Удалить файл из папки")
    @Test
    public void deleteFileFromFolder() throws Exception {
        var psd = new PagePsd(driver);
        psd.selectRandomFolder();
        psd.deleteFile();
        psd.deleteFile();
    }

    @DisplayName("Импортировать смету")
    @Test
    public void importEstimate() throws Exception {
        var psd = new PagePsd(driver);
        psd.selectRandomFolder();
        psd.importSomeEstimatesInPSD();
    }

    @DisplayName("")
    @Test
    public void check() throws Exception {
        startTest();
        var psd = new PagePsd(driver);
        var feed = new PageFeed(driver);
        var project = new PageProject(driver);
        /*feed.enterInSystem(testUserEmail, testUserPassword2);
        project.openProjectPage();*/
        project.chooseProject("123");
        Thread.sleep(18000);
        psd.openPSD();
        psd.createNewFolder(2);
        psd.editRandomFolder();
        psd.deleteRandomFolder();
        psd.addFolderToFolder(addFolder);
        psd.selectRandomFolder();
        psd.loadFile(estimateLink1);
        psd.loadFile(estimateLink2);
        psd.loadFile(estimateLink3);
        psd.loadFile(estimateLink4);
        psd.loadFile(estimateLink5);
        psd.importSomeEstimatesInPSD();
    }
}
