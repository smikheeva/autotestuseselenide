import Helpers.BaseTest;
import Parts.PageFeed;
import Parts.PageNotebook;
import Parts.PageProject;
import Parts.PageWorkProgress;
import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;


public class ProgressWorkTest extends BaseTest {

    private String testUserEmail = "test@ya.ru";
    private String testUserPassword = "Qwerty123";
    private String testUserPassword2 = "123456";

    private String estimate = "Благоустройство и озеленение территории";
    private String mainWork = "Разборка надземной части без сохранения годных материалов: кирпичных зданий 1, 2-этажных";

    private String estimate2 = "МЕТАЛЛОКОНСТРУКЦИИ ПОКРЫТИЯ ЧАШИ БАССЕЙНА";
    private String addWork1 = "Тестовая 1";
    private String addWork2 = "Тестовая 2";

    private String estimate3 = "ГИДРОИЗОЛЯЦИЯ И ОТДЕЛКА ОБХОДНЫХ ДОРОЖЕК";

    private String myComment = "Петрович! Когда уже нормальные трубы привезут?";
    private String fileName = "/home/user/Изображения/Строительные работы/little_jpg.jpg";
    private String volumeWork1 = Double.toString(Precision.round((Math.random() + 0.1) / 10, 4));
    private String volumeWork2 = Double.toString(Precision.round((Math.random() + 0.1) / 10, 4));
    private String titleNewWork = "Демонтаж дополнительных труб 5";

    @DisplayName("Проверить отображение смет")
    @Test
    public void checkDisplayEstimates() throws Exception {
        //startTest();
        var feed = new PageFeed(driver);
        var work = new PageWorkProgress(driver);
        var project = new PageProject(driver);
        //feed.enterInSystem(testUserEmail, testUserPassword);
        project.openProjectPage();
        project.chooseProject("123");
        Thread.sleep(4000);
        work.openProgressWork();

    }

    @DisplayName("Проверить отображение работ")
    @Test
    public void checkDisplayWorks() throws Exception {
        var work = new PageWorkProgress(driver);
        work.selectRandomEstimate();
    }

    @DisplayName("Отменить создания дополнительной работы")
    @Test
    public void cancelCreatingNewWork() throws Exception {
        var work = new PageWorkProgress(driver);
        work.selectRandomEstimate();
        work.cancelCreatingNewWorkWithoutComment("Отменяемая тестовая дополнительная работа");
    }

    @DisplayName("Подтвердить создание дополнительной работы")
    @Test
    public void confirmCreatingNewWork() throws Exception {
        var work = new PageWorkProgress(driver);
        work.selectRandomEstimate();
        work.confirmCreatingNewWorkWithoutComment("Тестовая дополнительная работа");
    }

    @DisplayName("Добавить запись о выполнении объёма работы больше планового")
    @Test
    public void addRecordOfWorkWithVolumeMoreThenPlaned() throws Exception {
        var work = new PageWorkProgress(driver);
        work.selectRandomEstimateAndWork();
        work.addVolumeMoreThenPlaned();
    }

    @DisplayName("Добавить запись о выполнении отрицательного объёма работы")
    @Test
    public void addRecordOfWorkWithVolumeSubzero() throws Exception {
        var work = new PageWorkProgress(driver);
        work.returnToListOfEstimates();
        work.selectRandomEstimateAndWork();
        work.addVolumeSubzero();
    }

    @DisplayName("Отменить добавление записи о выполнении валидного объёма работы")
    @Test
    public void cancelAddingRecordOfWork() throws Exception {
        var work = new PageWorkProgress(driver);
        work.returnToListOfEstimates();
        work.selectRandomEstimateAndWork();
        work.cancelAddingRecordToWork();
    }

    @DisplayName("Подтвердить добавление записи о выполнении валидного объёма работы")
    @Test
    public void addRecordOfWork() throws Exception {
        var work = new PageWorkProgress(driver);
        work.selectRandomEstimateAndWork();
        work.confirmAddingRecordOnWork();

    }


    /**
     * Выбираю смету, выбираю случайную работу, добавляю запись
     */
    @Test
    public void check() throws Exception {
        startTest();
        var feed = new PageFeed(driver);
        var note = new PageNotebook(driver);
        var work = new PageWorkProgress(driver);
        var project = new PageProject(driver);
        /*feed.enterInSystem(testUserEmail, testUserPassword2);
        project.openProjectPage();
        project.chooseProject("123");
        Thread.sleep(4000);*/
        work.openProgressWork();
        work.selectRandomEstimateAndWork();
        work.confirmAddingRecordOnWork();
        work.returnToListOfEstimates();
        work.selectRandomEstimateAndWork();
        work.confirmAddingRecordOnWork();
        work.returnToListOfEstimates();
        work.selectRandomEstimateAndWork();
        work.confirmAddingRecordOnWork();
        note.openNotebook();
        note.cancelConfirmWork();
        note.uncheckAllWorks();
        note.clickRandomCheckboxWork();
        note.clickRandomCheckboxWork();
        note.confirmWork();
        Thread.sleep(2000);
        //note.checkedSystemMessage();
    }

    @Test
    public void check1() throws Exception{
        startTest();
        var note = new PageNotebook(driver);
        var work = new PageWorkProgress(driver);
        var project = new PageProject(driver);
        //project.openProjectPage();
        //project.chooseProject("Мой новый тестовый проект");
        work.openProgressWork();
        work.check();
    }

}

