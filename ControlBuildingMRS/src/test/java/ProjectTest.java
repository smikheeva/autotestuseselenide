import Helpers.BaseTest;
import Parts.PageFeed;
import Parts.PageProject;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;


public class ProjectTest extends BaseTest {
    private String adminEmail = "test@ya.ru";
    private String hcqcEmail = "hcqc@ya.ru";
    private String iaEmail = "ia@ya.ru";
    private String eptdEmail = "eptd@ya.ru";
    private String foremanEmail = "foreman@ya.ru";
    private String ecqcEmail = "ecqc@ya.ru";

    private String Password = "Qwerty123";
    private String Password2 = "123456";


    private String ProjectName     = "Школа";
    private String EditProjectName = "123";
    private String AddressBuilding = "Новый адрес";
    private String FileName = "/home/user/Изображения/Строительные работы/мал_jpg.jpg";

    @DisplayName ("Войти как администратор")
    @Test
    public void enterAdmin() throws Exception {
        startTest();
        var  project = new PageProject(driver);
        project.enterInSystem(adminEmail, Password);
    }

    @DisplayName ("Войти как руководитель СК")
    @Test
    public void enterHCQC() throws Exception {
        startTest();
        var  project = new PageProject(driver);
        project.enterInSystem(hcqcEmail, Password);
    }

    @DisplayName ("Войти как инженер СК")
    @Test
    public void enterECQC() throws Exception {
        startTest();
        var  project = new PageProject(driver);
        project.enterInSystem(ecqcEmail, Password);
    }

    @DisplayName ("Войти как инженер ПТО")
    @Test
    public void enterEPTD() throws Exception {
        startTest();
        var  project = new PageProject(driver);
        project.enterInSystem(eptdEmail, Password);
    }

    @DisplayName ("Войти как прораб")
    @Test
    public void enterForeman() throws Exception {
        startTest();
        var  project = new PageProject(driver);
        project.enterInSystem(foremanEmail, Password);
    }

    @DisplayName ("Войти как исполнитель")
    @Test
    public void enterIA() throws Exception {
        startTest();
        var  project = new PageProject(driver);
        project.enterInSystem(iaEmail, Password);
    }

    @DisplayName ("Создать проект и отменить")
    @Test
    public void cancelCreatingProject() throws Exception {
        var  project = new PageProject(driver);
        project.openProjectPage();
        project.createProject();
        project.fillingFormNewProject(ProjectName, AddressBuilding);
        project.cancelCreateProject();
    }

    @DisplayName ("Создать проект и подтвердить")
    @Test
    public void confirmCreatingProject() throws Exception {
        var  project = new PageProject(driver);
        Thread.sleep(4000);
        project.createProject();
        project.fillingFormNewProject(ProjectName, AddressBuilding);
        project.confirmCreateProject(ProjectName);
        Thread.sleep(4000);
        project.checkProject(ProjectName);
    }

    @DisplayName ("Редактировать проект и отменить изменения")
    @Test
    public void cancelEditingProject() throws Exception {
        var  project = new PageProject(driver);
        project.cancelEditProjectAction(ProjectName, EditProjectName);
    }

    @DisplayName ("Редактировать проект и сохранить изменения")
    @Test
    public void saveEditingProject() throws Exception {
        var  project = new PageProject(driver);
        project.saveEditProjectAction(ProjectName, EditProjectName);
    }

    @DisplayName ("Удалить проект")
    @Test
    public void deleteProject() throws Exception {
        var  project = new PageProject(driver);
        project.createProject();
        project.fillingFormNewProject("Тестовый проект для удаления", AddressBuilding);
        project.confirmCreateProject("Тестовый проект для удаления");
        project.checkProject("Тестовый проект для удаления");
        project.deleteProjectAction("Тестовый проект для удаления");
    }

    @DisplayName ("Удалить проект 123")
    @Test
    public void deleteProject123() throws Exception {
        var  project = new PageProject(driver);
        project.openProjectPage();
        project.deleteProjectAction("123");
    }

    @DisplayName ("Добавить участника и отменить")
    @Test
    public void cancelAddingParticipants() throws Exception {
        var  project = new PageProject(driver);
        project.addParticipantsAction(ProjectName);
        project.fillingAddParticipantToProject();
        project.cancelAddParticipantToProject();
    }

    @DisplayName ("Добавить участника и подтвердить")
    @Test
    public void confirmAddingParticipants() throws Exception {
        var  project = new PageProject(driver);
        project.addParticipantsAction(ProjectName);
        project.fillingAddParticipantToProject();
        project.confirmAddParticipantToProject();
    }

    @DisplayName ("Выбрать некоторых сотрудников для добавления и отмеить")
    @Test
    public void cancelChoosingEmployeeForAdding() throws Exception {
        var  project = new PageProject(driver);
        project.addParticipantsAction(ProjectName);
        project.fillingAddParticipantToProject();
        project.confirmAddParticipantToProject();
        project.cancelAddEmployee();
    }

    @DisplayName ("Выбрать некоторых сотрудников для добавления и подтвердить")
    @Test
    public void confirmChoosingEmployeeForAdding() throws Exception {
        var  project = new PageProject(driver);
        project.addParticipantsAction(ProjectName);
        project.fillingAddParticipantToProject();
        project.confirmAddParticipantToProject();
        project.confirmAddEmployee();
    }

    @DisplayName ("Добавить участника и подтвердить, выбрать всех сотрудников для добавления и подтвердить")
    @Test
    public void confirmChoosingAllEmployeeForAdding() throws Exception {
        var  project = new PageProject(driver);
        project.addParticipantsAction(ProjectName);
        project.fillingAddParticipantToProject();
        project.confirmAddParticipantToProject();
        project.chooseAllEmployeeForAdding();
    }

    @DisplayName ("Отменить удаление участника")
    @Test
    public void cancelDeletingParticipant() throws Exception {
        var  project = new PageProject(driver);
        project.cancelDeleteParticipant();
    }

    @DisplayName ("Подтвердить удаление участника")
    @Test
    public void confirmDeletingParticipant() throws Exception {
        var  project = new PageProject(driver);
        project.confirmDeleteParticipant();
    }

    @DisplayName ("Для проверки")
    @Test
    public void check() throws Exception {
        startTest();
        var  project = new PageProject(driver);
        var  feed = new PageFeed(driver);
    }

}